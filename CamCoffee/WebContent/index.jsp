<%@page import="utility.Constant"%>
<html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
<script src="./js/jquery-3.3.1.min.js"></script>
<script src="./js/bootstrap.min.js"></script>
<title>Danh Sách Chức Năng</title>
<style>
a:hover {
	color: #959fae;
	text-decoration: none;
}

.glyphicon {
	font-size: 40px;
}

.feature-box {
	padding: 5px;
}

.col-md-12, .col-xs-12 {
	margin: 10px 0;
}
</style>
</head>
<body>
	<div class="container">
		<h2 class="text-center">Danh Sách Chức Năng</h2>
		<div class="col-md-12 col-xs-12 text-center">
			<div class="col-md-4 col-xs-4">
				<a href="./order/table.jsp"><span class="glyphicon glyphicon-list-alt"></span> <br>
					<%=Constant.CHOOSE_TABLE%></a>
			</div>
			<div class="col-md-4 col-xs-4">
				<a href="./order/list.jsp"><span class="glyphicon glyphicon-file"></span>
					<br> <%=Constant.LIST_ORDER%></a>
			</div>
			<div class="col-md-4 col-xs-4">
				<a href="./logout"><span class="glyphicon glyphicon-log-out"></span>
					<br> <%=Constant.LOGOUT%></a>
			</div>
		</div>
		<%
			String userNameSession = (String) request.getSession().getAttribute(Constant.USER_ENTITY);
			if (userNameSession.equalsIgnoreCase(Constant.ADMIN)) {
		%>
		<div class="col-md-12 col-xs-12 text-center">
			<div class="col-md-4 col-xs-4">
				<a href="./drinkType/list.jsp"><span
					class="glyphicon glyphicon-tint"></span> <br> <%=Constant.DRINK_TYPE%></a>
			</div>
			<div class="col-md-4 col-xs-4">
				<a href="./drink/list.jsp"><span
					class="glyphicon glyphicon-glass"></span> <br> <%=Constant.DRINK%></a>
			</div>
			<div class="col-md-4 col-xs-4">
				<a href="./user/list.jsp"><span class="glyphicon glyphicon-user"></span>
					<br> <%=Constant.USER%></a>
			</div>
		</div>
		<div class="col-md-12 col-xs-12 text-center">
			<div class="col-md-4 col-xs-4">
				<a href="./table/list.jsp"><span
					class="glyphicon glyphicon-credit-card"></span> <br> <%=Constant.TABLE%></a>
			</div>

			<div class="col-md-4 col-xs-4">
				<a href="./inCome/list.jsp"><span class="glyphicon glyphicon-usd"></span> <br>
					<%=Constant.MONEY%></a>
			</div>
		</div>
		<%
			}
		%>
	</div>
</body>
</html>