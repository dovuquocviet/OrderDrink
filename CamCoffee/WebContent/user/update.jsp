<!DOCTYPE html>
<%@page import="entity.UserEntity"%>
<%@page import="serviceImpl.UserServiceImpl"%>
<%@page import="service.UserService"%>
<%@page import="utility.Constant"%>
<html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<title>Sửa Đăng Ký</title>
<%
	String userNameSession = (String) request.getSession().getAttribute(Constant.USER_ENTITY);
	if (!userNameSession.equals(Constant.ADMIN)) {
		response.sendRedirect("../index.jsp");
	}
%>
</head>
<body>
	<div class="container">
		<form class="form-horizontal" method="POST" action="./update">
			<h2 class="text-center">Sửa Đăng Ký</h2>
			<%
				if (request.getAttribute(Constant.RESULT) != null) {
					int result = (Integer) request.getAttribute(Constant.RESULT);
					if (result == Constant.SUCCESS)
						out.print("<div class='text-center alert alert-success'>" + Constant.MESS_SUCCESS + "</div>");
					else
						out.print("<div class='text-center alert alert-danger'>" + Constant.MESS_FAILD + "</div>");
				}
			%>
			<div class="form-group">
				<label class="control-label col-md-4 col-xs-4">Tên Đăng Nhập</label>
				<div class="col-md-8 col-xs-8">
					<%
						String userName = "";
						int id = 0;
						if (request.getParameter("id") != null && request.getParameter("id").trim() != "") {
							id = Integer.parseInt(request.getParameter("id"));
							UserService userService = new UserServiceImpl();
							UserEntity userEntity = userService.findById(id);
							userName = userEntity.getUserName();
						} else {
							response.sendRedirect("list.jsp");
							return;
						}
					%>
					<input type="text" class="form-control" placeholder="Tên Đăng Nhập"
						name="userName" value=<%=userName%> readonly /> <input
						type="hidden" name="id" class="form-control" value=<%=id%> />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4 col-xs-4">Mật Khẩu</label>
				<div class="col-md-8 col-xs-8">
					<input type="password" class="form-control" placeholder="Mật Khẩu"
						name="password">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4 col-xs-4">Nhập Lại Mật
					Khẩu</label>
				<div class="col-md-8 col-xs-8">
					<input type="password" class="form-control"
						placeholder="Nhập Lại Mật Khẩu" name="rePassword">
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-offset-4 col-xs-offset-4 col-md-8 col-xs-8">
					<button class="btn btn-primary submit">Cập Nhật</button>
					<a href="./list.jsp">
						<button type="button" class="btn btn-default"><%=" " + Constant.BACK%></button>
					</a>
				</div>
			</div>
		</form>

	</div>
</body>
</html>