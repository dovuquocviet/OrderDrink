<!DOCTYPE html>
<%@page import="entity.UserEntity"%>
<%@page import="serviceImpl.UserServiceImpl"%>
<%@page import="service.UserService"%>
<%@page import="serviceImpl.DrinkTypeServiceImpl"%>
<%@page import="service.DrinkTypeService"%>
<%@page import="utility.Constant"%>
<%@page import="entity.DrinkTypeEntity"%>
<%@page import="java.util.List"%>
<html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<title>Danh Sách Nhân Viên</title>
<style>
a, a:hover {
	color: white;
}
</style>
<%
	String userNameSession = (String) request.getSession().getAttribute(Constant.USER_ENTITY);
	if (!userNameSession.equals(Constant.ADMIN)) {
		response.sendRedirect("../index.jsp");
	}
%>
</head>
<body>
	<div class="container">
		<h2 class="text-center">Danh Sách Nhân Viên</h2>
		<div class="form-group">
			<a href="../index.jsp">
				<button type="button" class="btn btn-default btn-primary">
					<span class="glyphicon glyphicon-home"></span><%=" " + Constant.HOME%>
				</button>
			</a> <a href="./create.jsp">
				<button type="button" class="btn btn-default btn-primary">
					<span class="glyphicon glyphicon-plus"></span><%=" " + Constant.ADD_NEW%>
				</button>
			</a>
		</div>
		<div class="table-responsive">
			<table id="listDrinkType" class="table table-striped table-bordered ">
				<thead>
					<tr>
						<th><%=Constant.ID%></th>
						<th><%=Constant.NAME%></th>
						<th><%=Constant.EDIT%></th>
						<th><%=Constant.LOCK%></th>
						<th><%=Constant.UNLOCK%></th>
					</tr>
				</thead>
				<tbody>
					<%
						UserService userService = new UserServiceImpl();
						List<UserEntity> userServices = userService.getAll();
						int size = userServices.size();
						for (int i = 0; i < size; i++) {
					%>
					<tr>
						<td><%=userServices.get(i).getIsDelete() == Constant.DELETE
						? "<del>" + userServices.get(i).getId() + "</del>"
						: userServices.get(i).getId()%></td>
						<td><%=userServices.get(i).getIsDelete() == Constant.DELETE
						? "<del>" + userServices.get(i).getUserName() + "</del>"
						: userServices.get(i).getUserName()%></td>
						<td><a
							href="./update.jsp?id=<%=userServices.get(i).getId()%>">
								<button type="button" class="btn btn-default btn-warning">
									<span class="glyphicon glyphicon-edit"></span>
								</button>
						</a></td>
						<td><input type="hidden"
							value="<%=userServices.get(i).getId()%>" />
							<button type="button" class="btn btn-default btn-danger btnLock"
								<%=userServices.get(i).getIsDelete() == Constant.DELETE ? "disabled" : ""%>>
								<span class="glyphicon glyphicon-lock"></span>
							</button></td>
						<td><input type="hidden"
							value="<%=userServices.get(i).getId()%>" />
							<button type="button"
								class="btn btn-default btn-success btnUnLock"
								<%=userServices.get(i).getIsDelete() != Constant.DELETE ? "disabled" : ""%>>
								<span class="glyphicon glyphicon-ok"></span>
							</button></td>
					</tr>
					<%
						}
					%>
				</tbody>
			</table>
		</div>
	</div>

	<!-- Model lock user -->
	<div class="modal fade" id="myModalLock" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header bg-danger">
					<h4 class="modal-title"><%="Xác Nhận"%></h4>
				</div>
				<div class="modal-body">
					<h4 class="text-center text-danger"><%=Constant.MESS_LOCK%></h4>
				</div>
				<div class="modal-footer">
					<button type="button"
						class="btn btn-default btn-danger confirmLock"
						data-dismiss="modal"><%=Constant.YES%></button>
					<button type="button" class="btn btn-default" data-dismiss="modal"><%=Constant.NO%></button>
				</div>
			</div>
		</div>
	</div>

	<!-- Model unlock user -->
	<div class="modal fade" id="myModalUnLock" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header bg-success">
					<h4 class="modal-title"><%="Xác Nhận"%></h4>
				</div>
				<div class="modal-body">
					<h4 class="text-center text-success"><%=Constant.MESS_UNLOCK%></h4>
				</div>
				<div class="modal-footer">
					<button type="button"
						class="btn btn-default btn-success confirmUnLock"
						data-dismiss="modal"><%=Constant.YES%></button>
					<button type="button" class="btn btn-default" data-dismiss="modal"><%=Constant.NO%></button>
				</div>
			</div>
		</div>
	</div>
</body>
<script src="../js/app/user/list.js"></script>
</html>