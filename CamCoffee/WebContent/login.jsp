<%@page import="utility.Constant"%>
<html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<title>Đăng Nhập</title>
</head>
<body>
	<div class="container">
		<form class="form-horizontal" method="POST" action="./login">
			<h2 class="text-center">Đăng Nhập</h2>
			<%
				if (request.getAttribute(Constant.RESULT) != null) {
					int result = (Integer) request.getAttribute(Constant.RESULT);
					if (result == Constant.FAILD)
						out.print("<div class='text-center alert alert-danger'>" + Constant.MESS_LOGIN_FAILD + "</div>");
					else
						out.print("<div class='text-center alert alert-success'>" + Constant.MESS_LOGIN_FAILD + "</div>");
				}
			%>
			<div class="form-group">
				<label class="control-label col-md-4 col-xs-4">Tên Đăng Nhập</label>
				<div class="col-md-8 col-xs-8">
					<input type="text" class="form-control" placeholder="Tên Đăng Nhập"
						name="userName">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4 col-xs-4">Mật Khẩu</label>
				<div class="col-md-8 col-xs-8">
					<input type="password" class="form-control" placeholder="Mật Khẩu"
						name="password">
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-offset-4 col-xs-offset-4 col-md-8 col-xs-8">
					<button class="btn btn-primary submit">Đăng Nhập</button>
				</div>
			</div>
		</form>
	</div>
</body>
</html>