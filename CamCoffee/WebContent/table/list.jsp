<!DOCTYPE html>
<%@page import="entity.TableEntity"%>
<%@page import="serviceImpl.TableServiceImpl"%>
<%@page import="service.TableService"%>
<%@page import="serviceImpl.DrinkTypeServiceImpl"%>
<%@page import="service.DrinkTypeService"%>
<%@page import="utility.Constant"%>
<%@page import="entity.DrinkTypeEntity"%>
<%@page import="java.util.List"%>
<html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<title>Danh Sách Bàn</title>
<style>
a, a:hover {
	color: white;
}
</style>
<%
	String userName = (String) request.getSession().getAttribute(Constant.USER_ENTITY);
	if (!userName.equals(Constant.ADMIN)) {
		response.sendRedirect("../index.jsp");
	}
%>
</head>
<body>
	<div class="container">
		<h2 class="text-center">Danh Sách Bàn</h2>
		<div class="form-group">
			<a href="../index.jsp">
				<button type="button" class="btn btn-default btn-primary">
					<span class="glyphicon glyphicon-home"></span><%=" " + Constant.HOME%>
				</button>
			</a> <a href="./create.jsp">
				<button type="button" class="btn btn-default btn-primary">
					<span class="glyphicon glyphicon-plus"></span><%=" " + Constant.ADD_NEW%>
				</button>
			</a>
		</div>
		<div class="table-responsive">
			<table id="listTable" class="table table-striped table-bordered ">
				<thead>
					<tr>
						<th><%=Constant.NAME%></th>
						<th><%=Constant.EDIT%></th>
						<th><%=Constant.REMOVE%></th>
					</tr>
				</thead>
				<tbody>
					<%
						TableService tableService = new TableServiceImpl();
						List<TableEntity> tableEntities = tableService.getAll();
						int size = tableEntities.size();
						for (int i = 0; i < size; i++) {
					%>
					<tr>
						<td><%=tableEntities.get(i).getName()%></td>
						<td><a
							href="./update.jsp?id=<%=tableEntities.get(i).getId()%>">
								<button type="button" class="btn btn-default btn-warning">
									<span class="glyphicon glyphicon-edit"></span>
								</button>
						</a></td>
						<td><input type="hidden"
							value="<%=tableEntities.get(i).getId()%>" />
							<button type="button"
								class="btn btn-default btn-danger btnDelete">
								<span class="glyphicon glyphicon-remove"></span>
							</button></td>
					</tr>
					<%
						}
					%>
				</tbody>
			</table>
		</div>
	</div>

	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header bg-danger">
					<h4 class="modal-title"><%="Xác Nhận"%></h4>
				</div>
				<div class="modal-body">
					<h4 class="text-center text-danger"><%=Constant.MESS_CONFIRM%></h4>
				</div>
				<div class="modal-footer">
					<button type="button"
						class="btn btn-default btn-danger confirmDelete"
						data-dismiss="modal"><%=Constant.YES%></button>
					<button type="button" class="btn btn-default" data-dismiss="modal"><%=Constant.NO%></button>
				</div>
			</div>
		</div>
	</div>
</body>
<script src="../js/app/table/list.js"></script>
</html>