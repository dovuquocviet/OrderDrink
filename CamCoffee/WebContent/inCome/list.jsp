<!DOCTYPE html>
<%@page import="entity.UserEntity"%>
<%@page import="serviceImpl.UserServiceImpl"%>
<%@page import="service.UserService"%>
<%@page import="dto.InComeDto"%>
<%@page import="serviceImpl.InComeServiceImpl"%>
<%@page import="service.InComeService"%>
<%@page import="utility.Constant"%>
<%@page import="java.util.List"%>
<html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/moment.js"></script>
<script type="text/javascript"
	src="../js/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="../js/bootstrap-datetimepicker.min.css" />
<title>Danh Sách Doanh Thu</title>
<style>
a, a:hover {
	color: white;
}
</style>
<%
	String userName = (String) request.getSession().getAttribute(Constant.USER_ENTITY);
	if (!userName.equals(Constant.ADMIN)) {
		response.sendRedirect("../index.jsp");
	}
%>
</head>
<body>
	<div class="container">
		<h2 class="text-center">Danh Sách Doanh Thu</h2>
		<div class="form-group">
			<a href="../index.jsp">
				<button type="button" class="btn btn-default btn-primary">
					<span class="glyphicon glyphicon-home"></span><%=" " + Constant.HOME%>
				</button>
			</a> <a href="./list.jsp">
				<button type="button" class="btn btn-default btn-primary">
					<span class="glyphicon glyphicon-refresh"></span><%=" " + Constant.INCOME_MONTH%>
				</button>
			</a>
		</div>
		<div class="form-horizontal">
			<div class="form-group">
				<label class="control-label col-md-2 col-xs-2"><%=Constant.START_DATE%></label>
				<div class="col-md-4 col-xs-4">
					<input type='text' class="form-control date" name="startDate" />
				</div>
				<label class="control-label col-md-2 col-xs-2"><%=Constant.END_DATE%></label>
				<div class="col-md-4 col-xs-4">
					<input type='text' class="form-control date" name="endDate" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2 col-xs-2"><%=Constant.STAFF%></label>
				<div class="col-md-4 col-xs-4">
					<select class="form-control" name="staffName">
						<option></option>
						<option value=<%=Constant.ADMIN%>><%=Constant.ADMIN%></option>
						<%
							UserService userService = new UserServiceImpl();
							List<UserEntity> userEntities = userService.getAll();
							int size = userEntities.size();
							for (int i = 0; i < size; i++) {
						%>
						<option value=<%=userEntities.get(i).getUserName()%>><%=userEntities.get(i).getUserName()%></option>
						<%
							}
						%>
					</select>
				</div>
				<div class="col-md-6 col-xs-6">
					<button class="btn btn-primary submit pull-right">
						<span class="glyphicon glyphicon-search"></span><%=" " + Constant.SEARCH%>
					</button>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-xs-3"><%=Constant.TOTAL_AMOUNT + ": "%></label>
				<label class="control-label col-md-3 col-xs-3" id="totalAmount"></label>
			</div>
		</div>
		<div class="table-responsive">
			<table id="listInCome" class="table table-striped table-bordered ">
				<thead>
					<tr>
						<th><%=Constant.ID%></th>
						<th><%=Constant.DRINK%></th>
						<th><%=Constant.TABLE%></th>
						<th><%=Constant.STAFF%></th>
						<th><%=Constant.DATE%></th>
						<th><%=Constant.COST%></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</body>
<script src="../js/app/inCome/list.js"></script>
</html>