<!DOCTYPE html>
<%@page import="utility.Constant"%>
<%@page import="entity.DrinkTypeEntity"%>
<%@page import="serviceImpl.DrinkTypeServiceImpl"%>
<%@page import="service.DrinkTypeService"%>
<html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<title>Loại Đồ Uống</title>
<%
	String userName = (String) request.getSession().getAttribute(Constant.USER_ENTITY);
	if (!userName.equals(Constant.ADMIN)) {
		response.sendRedirect("../index.jsp");
	}
%>
</head>
<body>
	<div class="container">
		<h2 class="text-center">Cập Nhật Loại Đồ Uống</h2>
		<form class="form-horizontal" action="./update" method="POST">
			<%
				if (request.getAttribute(Constant.RESULT) != null) {
					int result = (Integer) request.getAttribute(Constant.RESULT);
					if (result == Constant.SUCCESS)
						out.print("<div class='text-center alert alert-success'>" + Constant.MESS_SUCCESS + "</div>");
					else
						out.print("<div class='text-center alert alert-danger'>" + Constant.MESS_FAILD + "</div>");
				}
			%>
			<div class="form-group">
				<label class="control-label col-md-4 col-xs-4"><%=Constant.NAME%></label>
				<div class="col-md-8 col-xs-8">
					<%
						String name = "";
						int id = 0;
						if (request.getParameter("id") != null && request.getParameter("id").trim() != "") {
							id = Integer.parseInt(request.getParameter("id"));
							DrinkTypeService drinkTypeService = new DrinkTypeServiceImpl();
							DrinkTypeEntity drinkTypeEntity = drinkTypeService.findById(id);
							if (drinkTypeEntity == null) {
								response.sendRedirect("list.jsp");
							} else {
								name = drinkTypeEntity.getName();
							}
						} else {
							response.sendRedirect("list.jsp");
							return;
						}
					%>
					<input type="hidden" name="id" value="<%=id%>" /> <input
						type="text" class="form-control" name="name" value="<%=name%>" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-offset-4 col-xs-offset-4 col-md-8 col-xs-8">
					<button class="btn btn-primary submit"><%=Constant.UPDATE%></button>
					<a href="./list.jsp">
						<button type="button" class="btn btn-default"><%=" " + Constant.BACK%></button>
					</a>
				</div>

			</div>
		</form>
	</div>
</body>
</html>