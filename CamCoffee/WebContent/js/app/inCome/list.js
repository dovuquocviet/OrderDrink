$(document).ready(function() {
	calTotalAmont();
	
	$.ajax({
		url : '../inCome/getInCome',
		type : "GET",
		success : function(data) {
			var table = $('#listInCome tbody');
			table.find('tr').remove();
			$.each(data, function(index, item) {
				var row = '<tr>';
				row += '<td>' + item.orderId + '</td>';
				row += '<td>' + item.drinkName + '</td>';
				row += '<td>' + item.tableName + '</td>';
				row += '<td>' + item.userName + '</td>';
				row += '<td>' + item.createDate + '</td>';
				row += '<td class="cost">' + item.cost + '</td>';
				row += '</tr>';
				table.append(row);
			});
			calTotalAmont();
		},
		error : function() {
			alert('error');
		}
	});

	$('.submit').click(function() {
		var startDate = $('input[name="startDate"]').val();
		var endDate = $('input[name="endDate"]').val();
		var staffName = $('select option:selected').val();
		$.ajax({
			url : '../inCome/inComeCondition',
			type : "POST",
			data : {
				startDate : startDate,
				endDate : endDate,
				staffName : staffName
			},
			success : function(data) {
				var table = $('#listInCome tbody');
				table.find('tr').remove();
				$.each(data, function(index, item) {
					var row = '<tr>';
					row += '<td>' + item.orderId + '</td>';
					row += '<td>' + item.drinkName + '</td>';
					row += '<td>' + item.tableName + '</td>';
					row += '<td>' + item.userName + '</td>';
					row += '<td>' + item.createDate + '</td>';
					row += '<td class="cost">' + item.cost + '</td>';
					row += '</tr>';
					table.append(row);
				});
				calTotalAmont();
			},
			error : function() {
				alert('error');
			}
		});
	});

	$(function() {
		$('.date').datetimepicker({
			format : 'DD/MM/YYYY',
			maxDate : new Date
		});
	});
});

var calTotalAmont = function() {
	var totalAmount = 0;
	$('#listInCome tbody .cost').each(function(index, item) {
		totalAmount += parseInt($(this).html());
	});
	$('#totalAmount').html(totalAmount);
}