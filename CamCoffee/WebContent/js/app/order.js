$(document).ready(function() {
	$.ajax({
		url: './getOrder/',
		success: function(data){
			var objectJSON = jQuery.parseJSON(data);
			$.each(objectJSON, function(index, listDrink){
				var label = '<label>' + index + '</label>';
				var selectBox = '<select class="drink form-control">';
				$.each(listDrink, function(subIndex, drink){
					selectBox += '<option value="' + drink.id + '">';
					selectBox += drink.name;
					selectBox += '</option>';
				});
				selectBox += '</select>';
				var radio = '<label class="radio-inline"><input type="radio" name="' + index + ' value=0">Đá</label>';
				radio += '<label class="radio-inline"><input type="radio" name="' + index + ' value=1">Nóng</label>';
				var buttonAdd = '<button type="button" class="btn btn-success buttonAdd">';
				buttonAdd += "<span class='glyphicon glyphicon-ok'></span></button>";
				var group = "<div class='group col-md-12 col-xs-12'>" + label + selectBox + radio + buttonAdd + "</div>";
				var group = '<div class="group">' + label + selectBox + radio + buttonAdd + '</div>';
				$('#order').append(group);
			});
			
			var orders = [];
			$('#submit').click(function(){
				$('#listOrder tbody').find('tr').each(function(row){
					orders[row] = {
						'drink' : $(this).find('.drink').html(),
						'quantity' : $(this).find('.quantity').html(),
						'table' : 1,
						'type' : $(this).find('.type').val()
					};
				});
			});
			
			$('.buttonAdd').click(function(){
				var thisParent = $(this).parent();
				var type = thisParent.find('input[type=radio]:checked').val();
				var drinkId = thisParent.find('.drink option:selected').val();
				$.each(objectJSON, function(index, listDrink){
					$.each(listDrink, function(subIndex, drink){
						if(drink.id == drinkId){
							addTable(drink, type);
							//reset value
							thisParent.find('.drink').val(thisParent.find('.drink option:first').val());
						}
					});
				});
			});
			
			var addTable = function(drink, type){
				var row = "<tr>";
				row += "<td class='drink'>" + drink.name + "</td>";
				row += "<td class='quantity'>" + 1 + "</td>";
				row += "<td class='cost'>" + drink.cost + "</td>";
				if(type == 1){
					row += "<td><input type='hidden' value=1 class='type' />Nóng</td>";
				} else {
					row += "<td><input type='hidden' value=0 class='type' />Đá</td>";
				}
				row += "</tr>";
				$('#listOrder tbody').append(row);
			}
		},
		error: function(){
			alert('error');
		}
	});   
});