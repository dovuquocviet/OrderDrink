$(document).ready(function() {
	btnAction();
	setInterval(reload, 2000);
});

var reload = function() {
	$.ajax({
		url : '../order/list',
		type : "POST",
		data : {
			ids : getOrderId()
		},
		success : function(data) {
			if (data.length != 0) {
				new Audio('./DingSoundEffect.mp3').play();
				$.each(data, function(key, value) {
					var id = value.id;
					var drinkName = value.drinkName;
					var isCold = value.isCold == 0 ? "Nóng" : "Đá";
					var tableName = value.tableName;
					var row = "<tr>";
					row += "<td><input class='orderId' type='hidden' value='" + id + "'/>";
					row += drinkName + "</td>";
					row += "<td>" + isCold + "</td>;"
					row += "<td>" + tableName +"</td>";
					row += "<td><button type='button' class='btn btn-default btn-success'><span class='glyphicon glyphicon-ok'></span></button></td>";
					$("#listTable tbody").append(row);
				});
				btnAction();
			}
		},
		error : function() {
			alert('error');
		}
	});
}

var btnAction = function(){
	$(".btn-success").click(function() {
		var id = $(this).parents('tr').find('input[type="hidden"]').val();
		$.ajax({
			url : '../order/serviced',
			type : "POST",
			data : {
				id : id
			},
			success : function() {
				window.location.reload();
			},
			error : function() {
				alert('error');
			}
		});
	});
}

var getOrderId = function(){
	var json = [];
	$("#listTable tbody tr").each(function(index, item){
		json[index] = {
			'id': $(item).find('.orderId').val()
		}
	});
	return JSON.stringify(json);
}