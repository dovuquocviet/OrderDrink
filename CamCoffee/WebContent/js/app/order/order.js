$(document).ready(function() {
	calTotalAmont();
	
	$('.buttonAdd').click(function() {
		var thisParent = $(this).parents('div.form-group');
		var typeVal = 1;
		var typeName = "Đá";
		var checkBox = thisParent.find('input[type=checkbox]');
		if (checkBox.is(':checked')) {
			typeVal = 0;
			typeName = "Nóng";
		}
		// Get id and cost of drink
		var selectBoxValue = thisParent.find('.drink option:selected').val().split("_");
		var id = selectBoxValue[0];
		var cost = selectBoxValue[1];
		var name = thisParent.find('.drink option:selected').html();
		// add drink into table
		addTable(id, name, cost, typeVal, typeName);
		// remove checked check box
		checkBox.prop('checked', false);
		// calculator total amount
		calTotalAmont();
	});
	
	$('#pay').click(function(){
		var orderId = $("input[name='orderId']").first().val();
		var tableId = $("input[name='tableId']").val();
		$.ajax({
			url : '../bill/pay',
			type : "POST",
			data : {
				orderId : orderId,
				tableId : tableId
			},
			success: function(){
				$('#myModal').modal();
			},
			error : function() {
				alert('error');
			}
		});
	});
	
	$('#payDone').click(function(){
		window.location.reload();
	})

	$('#sendOrder').click(function() {
		var orders = {};
		var tableId = $('input[name="tableId"]').val();
		var drinks = [];
		$('#listOrder tbody').find('.new').each(function(row) {
			$(this).removeClass('new');
			drinks[row] = {
				'drinkId' : $(this).find('.drinkId').val(),
				'typeVal' : $(this).find('.typeVal').val()
			}
		});
		orders['tableId'] = tableId;
		orders['drinks'] = drinks;
		console.log(JSON.stringify(orders));
		
		$.ajax({
			url : '../order/order',
			type : "POST",
			data : {
				orders : JSON.stringify(orders)
			},
			success: function(){
				window.location.reload();
			},
			error : function() {
				alert('error');
			}
		});
	});
});

//add drink into table
var addTable = function(id, name, cost, typeVal, typeName) {
	var row = "<tr class='new'>";
	row += "<td class='name'>" + name;
	row += "<input type='hidden' class='drinkId' value='" + id + "'/></td>";
	row += "<td class='cost'>" + cost + "</td>";
	row += "<td class='typeName'>" + typeName;
	row += "<input type='hidden' class='typeVal' value='" + typeVal + "'/></td>";
	row += "<td class='delete'><button type='button' class='btn btn-default btn-danger btnDelete'><span class='glyphicon glyphicon-remove'></span></button></td>";
	row += "</tr>";
	$('#listOrder tbody').append(row);

	$('.btnDelete').click(function() {
		$(this).parents('tr').remove();
		calTotalAmont();
	});
}

var calTotalAmont = function() {
	var totalAmount = 0;
	$('#listOrder tbody .cost').each(function(index, item) {
		totalAmount += parseInt($(this).html());
	});
	$('#totalAmount').html(totalAmount);
}