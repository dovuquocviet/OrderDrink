$(document).ready(function() {
	var id = "";
	$(".btnDelete").click(function() {
		$("#myModal").modal();
		id = $(this).parents('td').find('input[type="hidden"]').val();
	});

	$(".confirmDelete").click(function() {
		$.ajax({
			url : '../drink/delete',
			type : "POST",
			data : {
				id : id
			},
			success: function(){
				window.location.reload();
			},
			error : function() {
				alert('error');
			}
		});
	});
});