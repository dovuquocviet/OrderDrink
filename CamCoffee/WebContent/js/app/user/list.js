$(document).ready(function() {
	var id = "";
	$(".btnLock").click(function() {
		$("#myModalLock").modal();
		id = $(this).parents('td').find('input[type="hidden"]').val();
	});

	$(".btnUnLock").click(function() {
		$("#myModalUnLock").modal();
		id = $(this).parents('td').find('input[type="hidden"]').val();
	});

	$(".confirmLock").click(function() {
		$.ajax({
			url : '../user/lock',
			type : "POST",
			data : {
				id : id
			},
			success : function() {
				window.location.reload();
			},
			error : function() {
				alert('error');
			}
		});
	});

	$(".confirmUnLock").click(function() {
		$.ajax({
			url : '../user/unlock',
			type : "POST",
			data : {
				id : id
			},
			success : function() {
				window.location.reload();
			},
			error : function() {
				alert('error');
			}
		});
	});
});