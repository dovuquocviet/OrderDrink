var SUCCESS = 1;
$(document).ready(function() {
	$('.submit').click(function() {
		var userName = $('input[name="userName"').val().trim();
		var password = $('input[name="password"').val().trim();
		var rePassword = $('input[name="rePassword"').val().trim();
		if (userName == '' || password == '' || rePassword == '') {
			var alert = "Không Để Trống";
			$('.alert').removeClass("alert-success");
			$('.alert').addClass("alert-danger");
			$('.alert').html(alert);
		} else if(password != rePassword){
			var alert = "Hai Mật Khẩu Phải Trùng";
			$('.alert').removeClass("alert-success");
			$('.alert').addClass("alert-danger");
			$('.alert').html(alert);
		} else {
			$.ajax({
				url : '../user/create',
				type : "POST",
				data : {
					userName : userName,
					password : password,
					rePassword : rePassword
				},
				success : function(data) {
					if (data == SUCCESS) {
						var alert = "Tạo Mới Thành Công";
						$('.alert').removeClass("alert-danger");
						$('.alert').addClass("alert-success");
						$('.alert').html(alert);
					} else {
						var alert = "Tạo Mới Thất Bại";
						$('.alert').removeClass("alert-success");
						$('.alert').addClass("alert-danger");
						$('.alert').html(alert);
					}
					$('input[name="userName"').val('');
					$('input[name="password"').val('');
					$('input[name="rePassword"').val('');
				},
				error : function() {
					alert('error');
				}
			});
		}
	});
});