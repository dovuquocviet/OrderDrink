<!DOCTYPE html>
<%@page import="serviceImpl.DrinkServiceImpl"%>
<%@page import="dto.DrinkDto"%>
<%@page import="java.util.List"%>
<%@page import="service.DrinkService"%>
<%@page import="utility.Constant"%>
<html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<title>Danh Sách Đồ Uống</title>
<style>
a, a:hover {
	color: white;
}
</style>
<%
	String userName = (String) request.getSession().getAttribute(Constant.USER_ENTITY);
	if (!userName.equals(Constant.ADMIN)) {
		response.sendRedirect("../index.jsp");
	}
%>
</head>
<body>
	<div class="container">
		<h2 class="text-center">Danh Sách Đồ Uống</h2>
		<div class="form-group">
			<a href="../index.jsp">
				<button type="button" class="btn btn-default btn-primary">
					<span class="glyphicon glyphicon-home"></span><%=" " + Constant.HOME%>
				</button>
			</a> <a href="./create.jsp">
				<button type="button" class="btn btn-default btn-primary">
					<span class="glyphicon glyphicon-plus"></span><%=" " + Constant.ADD_NEW%>
				</button>
			</a>
		</div>
		<div class="table-responsive">
			<table id="listDrinkType" class="table table-striped table-bordered ">
				<thead>
					<tr>
						<th><%=Constant.NAME%></th>
						<th><%=Constant.TYPE%></th>
						<th><%=Constant.COST%></th>
						<th><%=Constant.EDIT%></th>
						<th><%=Constant.REMOVE%></th>
					</tr>
				</thead>
				<tbody>
					<%
						DrinkService drinkService = new DrinkServiceImpl();
						List<DrinkDto> drinkDtos = drinkService.getAll();
						int size = drinkDtos.size();
						for (int i = 0; i < size; i++) {
					%>
					<tr>
						<td><%=drinkDtos.get(i).getName()%></td>
						<td><%=drinkDtos.get(i).getDrinkTypeName()%></td>
						<td><%=drinkDtos.get(i).getCost()%></td>
						<td><a href="./update.jsp?id=<%=drinkDtos.get(i).getId()%>">
								<button type="button" class="btn btn-default btn-warning">
									<span class="glyphicon glyphicon-edit"></span>
								</button>
						</a></td>
						<td><input type="hidden"
							value="<%=drinkDtos.get(i).getId()%>" />
							<button type="button"
								class="btn btn-default btn-danger btnDelete">
								<span class="glyphicon glyphicon-remove"></span>
							</button></td>
					</tr>
					<%
						}
					%>
				</tbody>
			</table>
		</div>
	</div>

	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header bg-danger">
					<h4 class="modal-title"><%="Xác Nhận"%></h4>
				</div>
				<div class="modal-body">
					<h4 class="text-center text-danger"><%=Constant.MESS_CONFIRM%></h4>
				</div>
				<div class="modal-footer">
					<button type="button"
						class="btn btn-default btn-danger confirmDelete"
						data-dismiss="modal"><%=Constant.YES%></button>
					<button type="button" class="btn btn-default" data-dismiss="modal"><%=Constant.NO%></button>
				</div>
			</div>
		</div>
	</div>
</body>
<script src="../js/app/drink/list.js"></script>
</html>