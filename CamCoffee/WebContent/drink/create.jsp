<!DOCTYPE html>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="entity.DrinkTypeEntity"%>
<%@page import="java.util.List"%>
<%@page import="daoImpl.DrinkTypeDaoImpl"%>
<%@page import="dao.DrinkTypeDao"%>
<%@page import="utility.Constant"%>
<html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<title>Đồ Uống</title>
<%
	String userName = (String) request.getSession().getAttribute(Constant.USER_ENTITY);
	if (!userName.equals(Constant.ADMIN)) {
		response.sendRedirect("../index.jsp");
	}
%>
</head>
<body>
	<div class="container">
		<form class="form-horizontal" method="post" action="./create">
			<h2 class="text-center">Tạo Đồ Uống</h2>
			<%
				if (request.getAttribute(Constant.RESULT) != null) {
					int result = (Integer) request.getAttribute(Constant.RESULT);
					if (result == Constant.SUCCESS)
						out.print("<div class='text-center alert alert-success'>" + Constant.MESS_SUCCESS + "</div>");
					else
						out.print("<div class='text-center alert alert-danger'>" + Constant.MESS_FAILD + "</div>");
				}
			%>
			<div class="form-group">
				<label class="control-label col-md-4 col-xs-4"><%=Constant.NAME%></label>
				<div class="col-md-8 col-xs-8">
					<input type="text" class="form-control"
						placeholder="<%=Constant.NAME%>" name="name">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4 col-xs-4"><%=Constant.TYPE%></label>
				<div class="col-md-8 col-xs-8">
					<select class="form-control" name="drinkTypeId">
						<%
							DrinkTypeDao drinkTypeDao = new DrinkTypeDaoImpl();
							List<DrinkTypeEntity> drinkTypeEntities = drinkTypeDao.getAll();
							int size = drinkTypeEntities.size();
							for (int i = 0; i < size; i++) {
						%>
						<option value="<%=drinkTypeEntities.get(i).getId()%>"><%=drinkTypeEntities.get(i).getName()%></option>
						<%
							}
						%>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4 col-xs-4"><%=Constant.COST%></label>
				<div class="col-md-8 col-xs-8">
					<input type="text" class="form-control"
						placeholder="<%=Constant.COST%>" name="cost">
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-offset-4 col-xs-offset-4 col-md-8 col-xs-8">
					<button class="btn btn-primary submit"><%=Constant.ADD_NEW%></button>
					<a href="./list.jsp">
						<button type="button" class="btn btn-default"><%=" " + Constant.BACK%></button>
					</a>
				</div>
			</div>
		</form>
	</div>
</body>
</html>