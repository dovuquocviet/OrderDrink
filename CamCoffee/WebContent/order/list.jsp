<%@page import="dto.OrderListDto"%>
<%@page import="serviceImpl.OrderServiceImpl"%>
<%@page import="service.OrderService"%>
<%@page import="java.util.List"%>
<%@page import="utility.Constant"%>
<html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<title>Danh Sách Gọi Đồ</title>
<style type="text/css">
.row {
	margin: 10px 0;
}
</style>
</head>
<body>
	<div class="container">
		<h2 class="text-center"><%=Constant.LIST_ORDERS%></h2>
		<div class="form-group">
			<a href="../index.jsp">
				<button type="button" class="btn btn-default btn-primary">
					<span class="glyphicon glyphicon-home"></span><%=" " + Constant.HOME%>
				</button>
			</a>
		</div>
		<div class="table-responsive">
			<table id="listTable" class="table table-striped table-bordered ">
				<thead>
					<tr>
						<th><%=Constant.NAME%></th>
						<th><%=Constant.TYPE%></th>
						<th><%=Constant.TABLE%></th>
						<th><%=Constant.SERVICED%></th>
					</tr>
				</thead>
				<tbody>
					<%
						OrderService orderService = new OrderServiceImpl();
						List<OrderListDto> orderListDtos = orderService.findOrder();
						int size = orderListDtos.size();
						for (int i = 0; i < size; i++) {
					%>
					<tr>
						<td><%=orderListDtos.get(i).getDrinkName()%> <input
							class="orderId" type="hidden"
							value=<%=orderListDtos.get(i).getId()%> /></td>
						<td><%=orderListDtos.get(i).getIsCold() == 0 ? Constant.HOT : Constant.COLD%></td>
						<td><%=orderListDtos.get(i).getTableName()%></td>
						<td>
							<button type="button" class="btn btn-default btn-success">
								<span class="glyphicon glyphicon-ok"></span>
							</button>
						</td>
					</tr>
					<%
						}
					%>

				</tbody>
			</table>
		</div>
	</div>
</body>
<script src="../js/app/order/list.js"></script>
</html>