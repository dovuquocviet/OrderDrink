<%@page import="dto.OrderDto"%>
<%@page import="entity.OrderEntity"%>
<%@page import="serviceImpl.OrderServiceImpl"%>
<%@page import="service.OrderService"%>
<%@page import="entity.DrinkEntity"%>
<%@page import="serviceImpl.DrinkServiceImpl"%>
<%@page import="service.DrinkService"%>
<%@page import="entity.DrinkTypeEntity"%>
<%@page import="java.util.List"%>
<%@page import="serviceImpl.DrinkTypeServiceImpl"%>
<%@page import="service.DrinkTypeService"%>
<%@page import="utility.Constant"%>
<%@page import="entity.TableEntity"%>
<%@page import="service.TableService"%>
<%@page import="serviceImpl.TableServiceImpl"%>
<html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<title>Gọi Đồ</title>
<style type="text/css">
.row {
	margin: 10px 0;
}
</style>
</head>
<body>
	<div class="container">
		<%
			String idParam = request.getParameter("tableId");
			if ("".equals(idParam)) {
				response.sendRedirect("./table.jsp");
				return;
			}
			int tableId = 0;
			try {
				tableId = Integer.parseInt(idParam);
			} catch (NumberFormatException e) {
				response.sendRedirect("./table.jsp");
				return;
			}
			TableService tableService = new TableServiceImpl();
			TableEntity tableEntity = tableService.findById(tableId);
			if (tableEntity == null) {
				response.sendRedirect("./table.jsp");
				return;
			}
		%>
		<h2 class="text-center"><%=Constant.ORDER + " " + tableEntity.getName()%></h2>
		<input type="hidden" value=<%=tableEntity.getId()%> name="tableId" />
		<input type="hidden"
			value=<%=request.getSession().getAttribute(Constant.USER_ENTITY)%>
			name="user" />
		<div class="form-group">
			<a href="./table.jsp">
				<button type="button" class="btn btn-default btn-primary">
					<span class="glyphicon glyphicon-list-alt"></span><%=" " + Constant.CHOOSE_TABLE%>
				</button>
			</a>
		</div>
		<form class="form-horizontal">
			<%
				DrinkTypeService drinkTypeService = new DrinkTypeServiceImpl();
				DrinkService drinkService = new DrinkServiceImpl();
				List<DrinkTypeEntity> drinkTypeEntities = drinkTypeService.getAll();
				int drinkTypeEntitiesSize = drinkTypeEntities.size();
				for (int i = 0; i < drinkTypeEntitiesSize; i++) {
			%>
			<div class="form-group">
				<label class="control-label col-md-3 col-xs-3"><%=drinkTypeEntities.get(i).getName()%></label>
				<input type="hidden" value=<%=drinkTypeEntities.get(i).getId()%>
					class="drinkTypeId" />
				<div class="col-md-5 col-xs-5">
					<select class="form-control drink">
						<%
							List<DrinkEntity> drinkEntities = drinkService.findByDrinkTypeId(drinkTypeEntities.get(i).getId());
								int drinkEntitiesSize = drinkEntities.size();
								for (int j = 0; j < drinkEntitiesSize; j++) {
						%>
						<option
							value=<%=drinkEntities.get(j).getId() + "_" + drinkEntities.get(j).getCost()%>><%=drinkEntities.get(j).getName()%></option>
						<%
							}
						%>
					</select>
				</div>
				<div class="col-md-2 col-xs-2 checkbox">
					<label><input type="checkbox" value="1"><%=Constant.HOT%></label>
				</div>
				<div class="col-md-1 col-xs-1">
					<button type="button" class="btn btn-success buttonAdd">
						<span class='glyphicon glyphicon-ok'></span>
					</button>
				</div>
			</div>
			<%
				}
			%>
			<div class="form-group">
				<label class="control-label col-md-3 col-xs-3"><%=Constant.TOTAL_AMOUNT + ": "%></label>
				<label class="control-label col-md-2 col-xs-2" id="totalAmount"></label>
				<button type="button" class="btn btn-success" id="sendOrder">
					<span class='glyphicon glyphicon-send'></span><%=" " + Constant.ORDER%>
				</button>
				<button type="button" class="btn btn-success" id="pay">
					<span class='glyphicon glyphicon-usd'></span><%=" " + Constant.PAY%>
				</button>
			</div>
		</form>
		<div class="table-responsive">
			<table id="listOrder" class="table table-striped table-bordered ">
				<thead>
					<tr>
						<th><%=Constant.NAME%></th>
						<th><%=Constant.COST%></th>
						<th><%=Constant.TYPE%></th>
						<th><%=Constant.REMOVE%></th>
					</tr>
				</thead>
				<tbody>
					<%
						OrderService orderService = new OrderServiceImpl();
						List<OrderDto> orderDtos = orderService.findOrderByTableId(tableId);
						int size = orderDtos.size();
						for (int i = 0; i < size; i++) {
					%>
					<tr>
						<td><%=orderDtos.get(i).getDrinkName()%><input type="hidden"
							value=<%=orderDtos.get(i).getDrinkId()%> name="drinkId" /><input
							type="hidden" value=<%=orderDtos.get(i).getId()%> name="orderId" /></td>
						<td class="cost"><%=orderDtos.get(i).getCost()%></td>
						<td><%=orderDtos.get(i).getIsCold() == 1 ? Constant.COLD : Constant.HOT%><input
							type="hidden" value=<%=orderDtos.get(i).getIsCold()%> /></td>
						<td class='delete'><button type='button'
								class='btn btn-default btn-danger btnDelete' disabled>
								<span class='glyphicon glyphicon-remove'></span>
							</button></td>
					</tr>
					<%
						}
					%>
				</tbody>
			</table>
		</div>
	</div>
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header bg-success">
					<h4 class="modal-title"><%="Thông Báo"%></h4>
				</div>
				<div class="modal-body">
					<h4 class="text-center text-success"><%=Constant.MESS_CONFIRM_PAY%></h4>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-success"
						id="payDone" data-dismiss="modal"><%=Constant.YES%></button>
					<button type="button" class="btn btn-default" data-dismiss="modal"><%=Constant.NO%></button>
				</div>
			</div>
		</div>
	</div>
</body>
<script src="../js/app/order/order.js"></script>
</html>