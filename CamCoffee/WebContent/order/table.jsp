<!DOCTYPE html>
<%@page import="entity.DrinkTypeEntity"%>
<%@page import="serviceImpl.DrinkTypeServiceImpl"%>
<%@page import="service.DrinkTypeService"%>
<%@page import="entity.TableEntity"%>
<%@page import="java.util.List"%>
<%@page import="serviceImpl.TableServiceImpl"%>
<%@page import="service.TableService"%>
<%@page import="utility.Constant"%>
<html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<title>Danh Sách Bàn</title>
<style>
a:hover {
	color: #959fae;
	text-decoration: none;
}

.col-md-4 .glyphicon, .col-xs-4 .glyphicon {
	font-size: 40px;
}

.feature-box {
	padding: 5px;
}

.col-md-4, .col-xs-4 {
	margin: 10px 0;
}
</style>
</head>
<body>
	<div class="container">
		<h2 class="text-center">Danh Sách Bàn</h2>
		<div class="form-group">
			<a href="../index.jsp">
				<button type="button" class="btn btn-default btn-primary">
					<span class="glyphicon glyphicon-home"></span><%=" " + Constant.HOME%>
				</button>
			</a>
		</div>
		<%
			TableService tableService = new TableServiceImpl();
			List<TableEntity> tableEntities = tableService.getAll();
			int size = tableEntities.size();
			for (int i = 0; i < size; i++) {
		%>
		<div class="col-md-4 col-xs-4 text-center">
			<a href="./order.jsp?tableId=<%=tableEntities.get(i).getId()%>"><span
				class="glyphicon glyphicon-shopping-cart"></span> <br> <%=tableEntities.get(i).getName()%></a>
		</div>
		<%
			}
		%>
		<%
			DrinkTypeService drinkTypeService = new DrinkTypeServiceImpl();
			List<DrinkTypeEntity> drinkTypeEntities = drinkTypeService.getAll();
		%>
	</div>
</body>
</html>