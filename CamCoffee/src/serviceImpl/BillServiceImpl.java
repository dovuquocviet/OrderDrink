package serviceImpl;

import java.util.Date;

import dao.BillDao;
import daoImpl.BillDaoImpl;
import entity.BillEntity;
import service.BillService;
import utility.Constant;

public class BillServiceImpl implements BillService {

	@Override
	public int create() {
		BillDao billDao = new BillDaoImpl();
		BillEntity billEntity = new BillEntity();
		billEntity.setCreateTime(new Date());
		billEntity.setIsPay(Constant.IS_NOT_PAY);
		return billDao.create(billEntity);
	}

	@Override
	public int pay(int id) {
		BillDao billDao = new BillDaoImpl();
		return billDao.pay(id);
	}

}
