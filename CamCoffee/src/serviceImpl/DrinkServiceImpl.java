package serviceImpl;

import java.util.List;

import dao.DrinkDao;
import daoImpl.DrinkDaoImpl;
import dto.DrinkDto;
import entity.DrinkEntity;
import service.DrinkService;

public class DrinkServiceImpl implements DrinkService {
	@Override
	public List<DrinkDto> getAll() {
		DrinkDao drinkDao = new DrinkDaoImpl();
		return drinkDao.getAll();
	}

	@Override
	public int create(DrinkEntity drinkEntity) {
		DrinkDao drinkDao = new DrinkDaoImpl();
		return drinkDao.create(drinkEntity);
	}

	@Override
	public DrinkDto findById(int id) {
		DrinkDao drinkDao = new DrinkDaoImpl();
		return drinkDao.findById(id);
	}

	@Override
	public int update(DrinkEntity drinkEntity) {
		DrinkDao drinkDao = new DrinkDaoImpl();
		return drinkDao.update(drinkEntity);
	}

	@Override
	public int delete(int id) {
		DrinkDao drinkDao = new DrinkDaoImpl();
		return drinkDao.delete(id);
	}

	@Override
	public List<DrinkEntity> findByDrinkTypeId(int drinkTypeId) {
		DrinkDao drinkDao = new DrinkDaoImpl();
		return drinkDao.findByDrinkTypeId(drinkTypeId);
	}
}
