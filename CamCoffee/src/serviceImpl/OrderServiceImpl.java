package serviceImpl;

import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import dao.OrderDao;
import daoImpl.OrderDaoImpl;
import dto.OrderDto;
import dto.OrderListDto;
import entity.OrderEntity;
import service.BillService;
import service.OrderService;
import utility.Constant;

public class OrderServiceImpl implements OrderService {

	@Override
	public int create(JSONObject jsonObject, String userName) {
		OrderDao orderDao = new OrderDaoImpl();
		BillService billService = new BillServiceImpl();
		int billId = billService.create();
		try {
			int tableId = Integer.parseInt(jsonObject.getString(Constant.TABLE_ID));
			JSONArray jsonArray = jsonObject.getJSONArray(Constant.DRINKS);
			int size = jsonArray.length();
			for (int i = 0; i < size; i++) {
				OrderEntity orderEntity = new OrderEntity();
				JSONObject jsonDrink = jsonArray.getJSONObject(i);
				int drinkId = jsonDrink.getInt(Constant.ID_DRINK);
				int typeVal = jsonDrink.getInt(Constant.TYPE_VAL);
				orderEntity.setBillId(billId);
				orderEntity.setDrinkId(drinkId);
				orderEntity.setIsCold(typeVal);
				orderEntity.setTableId(tableId);
				orderEntity.setUserName(userName);
				orderEntity.setCreateTime(new Date());
				orderDao.create(orderEntity);
			}
		} catch (JSONException e) {
			e.printStackTrace();
			return Constant.FAILD;
		}
		return Constant.SUCCESS;
	}

	@Override
	public List<OrderDto> findOrderByTableId(int tableId) {
		OrderDao orderDao = new OrderDaoImpl();
		return orderDao.findOrderByTableId(tableId);
	}

	@Override
	public int serviced(int id) {
		OrderDao orderDao = new OrderDaoImpl();
		return orderDao.serviced(id);
	}

	@Override
	public List<OrderListDto> findOrder() {
		OrderDao orderDao = new OrderDaoImpl();
		return orderDao.findOrder();
	}

	@Override
	public int findBillId(int id) {
		OrderDao orderDao = new OrderDaoImpl();
		return orderDao.findBillId(id);
	}
}
