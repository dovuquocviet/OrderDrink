package serviceImpl;

import java.util.List;

import dao.DrinkTypeDao;
import daoImpl.DrinkTypeDaoImpl;
import entity.DrinkTypeEntity;
import service.DrinkTypeService;

public class DrinkTypeServiceImpl implements DrinkTypeService {

	@Override
	public int create(DrinkTypeEntity drinkTypeEntity) {
		DrinkTypeDao drinkTypeDao = new DrinkTypeDaoImpl();
		return drinkTypeDao.create(drinkTypeEntity);
	}

	@Override
	public List<DrinkTypeEntity> getAll() {
		DrinkTypeDao drinkTypeDao = new DrinkTypeDaoImpl();
		return drinkTypeDao.getAll();
	}

	@Override
	public int update(DrinkTypeEntity drinkTypeEntity) {
		DrinkTypeDao drinkTypeDao = new DrinkTypeDaoImpl();
		return drinkTypeDao.update(drinkTypeEntity);
	}

	@Override
	public DrinkTypeEntity findById(int id) {
		DrinkTypeDao drinkTypeDao = new DrinkTypeDaoImpl();
		return drinkTypeDao.findById(id);
	}

	@Override
	public int delete(int id) {
		DrinkTypeDao drinkTypeDao = new DrinkTypeDaoImpl();
		return drinkTypeDao.delete(id);
	}
}
