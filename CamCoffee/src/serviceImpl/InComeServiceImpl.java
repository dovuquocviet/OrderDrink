package serviceImpl;

import java.util.List;

import dao.InComeDao;
import daoImpl.InComeDaoImpl;
import dto.InComeDto;
import service.InComeService;

public class InComeServiceImpl implements InComeService {

	@Override
	public List<InComeDto> getInCome() {
		InComeDao inComeDao = new InComeDaoImpl();
		return inComeDao.getInCome();
	}

	@Override
	public List<InComeDto> getInComeByTime(String startDate, String endDate) {
		InComeDao inComeDao = new InComeDaoImpl();
		return inComeDao.getInComeByTime(startDate, endDate);
	}

	@Override
	public List<InComeDto> getInComeByTimeAndUser(String startDate, String endDate, String userName) {
		InComeDao inComeDao = new InComeDaoImpl();
		return inComeDao.getInComeByTimeAndUser(startDate, endDate, userName);
	}

}
