package serviceImpl;

import java.util.List;

import dao.TableDao;
import daoImpl.TableDaoImp;
import entity.TableEntity;
import service.TableService;

public class TableServiceImpl implements TableService {

	@Override
	public int create(TableEntity tableEntity) {
		TableDao tableDao = new TableDaoImp();
		return tableDao.create(tableEntity);
	}

	@Override
	public List<TableEntity> getAll() {
		TableDao tableDao = new TableDaoImp();
		return tableDao.getAll();
	}

	@Override
	public int update(TableEntity tableEntity) {
		TableDao tableDao = new TableDaoImp();
		return tableDao.update(tableEntity);
	}

	@Override
	public TableEntity findById(int id) {
		TableDao tableDao = new TableDaoImp();
		return tableDao.findById(id);
	}

	@Override
	public int delete(int id) {
		TableDao tableDao = new TableDaoImp();
		return tableDao.delete(id);
	}
}
