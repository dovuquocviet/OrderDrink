package serviceImpl;

import java.util.List;

import dao.UserDao;
import daoImpl.UserDaoImpl;
import entity.UserEntity;
import service.UserService;

public class UserServiceImpl implements UserService {

	@Override
	public int create(UserEntity userEntity) {
		UserDao userDao = new UserDaoImpl();
		return userDao.create(userEntity);
	}

	@Override
	public int getUser(UserEntity userEntity) {
		UserDao userDao = new UserDaoImpl();
		return userDao.getUser(userEntity);
	}

	@Override
	public List<UserEntity> getAll() {
		UserDao userDao = new UserDaoImpl();
		return userDao.getAll();
	}

	@Override
	public int lock(int id) {
		UserDao userDao = new UserDaoImpl();
		return userDao.lock(id);
	}

	@Override
	public int unlock(int id) {
		UserDao userDao = new UserDaoImpl();
		return userDao.unlock(id);
	}

	@Override
	public UserEntity findById(int id) {
		UserDao userDao = new UserDaoImpl();
		return userDao.findById(id);
	}

	@Override
	public int update(UserEntity userEntity) {
		UserDao userDao = new UserDaoImpl();
		return userDao.update(userEntity);
	}

}
