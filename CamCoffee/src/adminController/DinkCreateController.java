package adminController;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.DrinkEntity;
import service.DrinkService;
import serviceImpl.DrinkServiceImpl;
import utility.Constant;

@WebServlet(name = "DinkCreateController", urlPatterns = { "/drink/create" })
public class DinkCreateController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6246111100383746716L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/drink/create.jsp").forward(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		DrinkService drinkService = new DrinkServiceImpl();
		DrinkEntity drinkEntity = new DrinkEntity();
		String name = request.getParameter("name").trim();
		String drinkTypeId = request.getParameter("drinkTypeId").trim();
		String cost = request.getParameter("cost").trim();
		if ("".equals(name) || "".equals(drinkTypeId) || "".equals(cost)) {
			request.setAttribute(Constant.RESULT, Constant.FAILD);
		} else {
			try {
				drinkEntity.setName(name);
				drinkEntity.setDrinkTypeId(Integer.parseInt(drinkTypeId));
				drinkEntity.setCost(Integer.parseInt(cost));
				request.setAttribute(Constant.RESULT, drinkService.create(drinkEntity));
			} catch (Exception e) {
				e.printStackTrace();
				request.setAttribute(Constant.RESULT, Constant.FAILD);
			}
		}
		request.getRequestDispatcher("/drink/create.jsp").forward(request, response);
	}
}
