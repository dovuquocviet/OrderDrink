package adminController;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.UserEntity;
import service.UserService;
import serviceImpl.UserServiceImpl;
import utility.Constant;

@WebServlet("/user/update")
public class UserUpdateController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9131656140420886493L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/user/update.jsp").forward(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		if ("".equals(request.getParameter("password")) || "".equals(request.getParameter("rePassword"))) {
			request.setAttribute(Constant.RESULT, Constant.FAILD);
		} else {
			try {
				UserService userService = new UserServiceImpl();
				int id = Integer.parseInt(request.getParameter("id"));
				String password = request.getParameter("password");
				String rePassword = request.getParameter("rePassword");
				if (password.equals(rePassword)) {
					UserEntity userEntity = new UserEntity();
					userEntity.setPassword(password);
					userEntity.setId(id);
					request.setAttribute(Constant.RESULT, userService.update(userEntity));
				} else {
					request.setAttribute(Constant.RESULT, Constant.FAILD);
				}
			} catch (Exception e) {
				e.printStackTrace();
				request.setAttribute(Constant.RESULT, Constant.FAILD);
			}
		}
		request.getRequestDispatcher("/user/update.jsp").forward(request, response);
	}
}
