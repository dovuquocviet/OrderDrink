package adminController;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.DrinkTypeEntity;
import service.DrinkTypeService;
import serviceImpl.DrinkTypeServiceImpl;
import utility.Constant;

@WebServlet("/drinkType/update")
public class DrinkTypeUpdateController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1941067176644745182L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/drinkType/update.jsp").forward(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		DrinkTypeEntity drinkTypeEntity = new DrinkTypeEntity();
		DrinkTypeService drinkTypeService = new DrinkTypeServiceImpl();
		String nameParam = request.getParameter("name").trim();
		String idParam = request.getParameter("id");
		if ("".equals(nameParam) || "".equals(idParam)) {
			request.setAttribute(Constant.RESULT, Constant.FAILD);
		} else {
			try {
				drinkTypeEntity.setName(nameParam);
				drinkTypeEntity.setId(Integer.parseInt(idParam));
				request.setAttribute(Constant.RESULT, drinkTypeService.update(drinkTypeEntity));
			} catch (Exception e) {
				e.printStackTrace();
				request.setAttribute(Constant.RESULT, Constant.FAILD);
			}
		}
		request.getRequestDispatcher("/drinkType/update.jsp").forward(request, response);
	}
}
