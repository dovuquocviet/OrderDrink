package adminController;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.TableEntity;
import service.TableService;
import serviceImpl.TableServiceImpl;
import utility.Constant;

@WebServlet("/table/create")
public class TableCreateController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7517423293425475086L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/table/create.jsp").forward(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		TableService tableService = new TableServiceImpl();
		TableEntity tableEntity = new TableEntity();
		String name = request.getParameter("name").trim();
		if ("".equals(name)) {
			request.setAttribute(Constant.RESULT, Constant.FAILD);
		} else {
			tableEntity.setName(name);
			request.setAttribute(Constant.RESULT, tableService.create(tableEntity));
		}
		request.getRequestDispatcher("/table/create.jsp").forward(request, response);
	}
}
