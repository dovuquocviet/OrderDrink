package adminController;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.UserService;
import serviceImpl.UserServiceImpl;
import utility.Constant;

@WebServlet("/user/unlock")
public class UserUnLockController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8530113791332089828L;

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		UserService userService = new UserServiceImpl();
		String paramId = request.getParameter("id").trim();
		if (!"".equals(paramId)) {
			try {
				int id = Integer.parseInt(paramId);
				userService.unlock(id);
			} catch (Exception e) {
				e.printStackTrace();
				request.setAttribute(Constant.RESULT, Constant.FAILD);
			}
		} else {
			request.setAttribute(Constant.RESULT, Constant.FAILD);
		}
		request.getRequestDispatcher("/drinkType/list.jsp").forward(request, response);
	}
}
