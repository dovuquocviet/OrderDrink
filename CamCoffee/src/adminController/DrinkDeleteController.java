package adminController;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.DrinkService;
import serviceImpl.DrinkServiceImpl;
import utility.Constant;

@WebServlet("/drink/delete")
public class DrinkDeleteController extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4381888579315399301L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/drink/list.jsp").forward(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DrinkService drinkService = new DrinkServiceImpl();
		String paramId = request.getParameter("id").trim();
		if (!"".equals(paramId)) {
			try {
				int id = Integer.parseInt(paramId);
				drinkService.delete(id);
			} catch (Exception e) {
				e.printStackTrace();
				request.setAttribute(Constant.RESULT, Constant.FAILD);
			}
		} else {
			request.setAttribute(Constant.RESULT, Constant.FAILD);
		}
		request.getRequestDispatcher("/drink/list.jsp").forward(request, response);
	}
}
