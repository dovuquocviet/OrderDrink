package adminController;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.DrinkTypeService;
import serviceImpl.DrinkTypeServiceImpl;
import utility.Constant;

@WebServlet("/drinkType/delete")
public class DrinkTypeDeleteController extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4381888579315399301L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/drinkType/list.jsp").forward(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DrinkTypeService drinkTypeService = new DrinkTypeServiceImpl();
		String paramId = request.getParameter("id").trim();
		if (!"".equals(paramId)) {
			try {
				int id = Integer.parseInt(paramId);
				drinkTypeService.delete(id);
			} catch (Exception e) {
				e.printStackTrace();
				request.setAttribute(Constant.RESULT, Constant.FAILD);
			}
		} else {
			request.setAttribute(Constant.RESULT, Constant.FAILD);
		}
		request.getRequestDispatcher("/drinkType/list.jsp").forward(request, response);
	}
}
