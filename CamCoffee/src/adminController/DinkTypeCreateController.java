package adminController;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.DrinkTypeEntity;
import service.DrinkTypeService;
import serviceImpl.DrinkTypeServiceImpl;
import utility.Constant;

@WebServlet("/drinkType/create")
public class DinkTypeCreateController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6246111100383746716L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/drinkType/create.jsp").forward(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		DrinkTypeService drinkTypeService = new DrinkTypeServiceImpl();
		DrinkTypeEntity drinkTypeEntity = new DrinkTypeEntity();
		String name = request.getParameter("name").trim();
		if ("".equals(name)) {
			request.setAttribute(Constant.RESULT, Constant.FAILD);
		} else {
			drinkTypeEntity.setName(name);
			request.setAttribute(Constant.RESULT, drinkTypeService.create(drinkTypeEntity));
		}
		request.getRequestDispatcher("/drinkType/create.jsp").forward(request, response);
	}
}
