package adminController;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.TableEntity;
import service.TableService;
import serviceImpl.TableServiceImpl;
import utility.Constant;

@WebServlet("/table/update")
public class TableUpdateController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6354813751924339299L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/table/update.jsp").forward(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		TableEntity tableEntity = new TableEntity();
		TableService tableService = new TableServiceImpl();
		String nameParam = request.getParameter("name").trim();
		String idParam = request.getParameter("id");
		if ("".equals(nameParam) || "".equals(idParam)) {
			request.setAttribute(Constant.RESULT, Constant.FAILD);
		} else {
			try {
				tableEntity.setName(nameParam);
				tableEntity.setId(Integer.parseInt(idParam));
				request.setAttribute(Constant.RESULT, tableService.update(tableEntity));
			} catch (Exception e) {
				e.printStackTrace();
				request.setAttribute(Constant.RESULT, Constant.FAILD);
			}
		}
		request.getRequestDispatcher("/table/update.jsp").forward(request, response);
	}
}
