package adminController;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;

import dto.InComeDto;
import service.InComeService;
import serviceImpl.InComeServiceImpl;

@WebServlet("/inCome/inComeCondition")
public class InComeCondition extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7775023037985728194L;

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String startDateParam = request.getParameter("startDate").trim();
		String endDateParam = request.getParameter("endDate").trim();
		String staffNameParam = request.getParameter("staffName").trim();
		InComeService inComeService = new InComeServiceImpl();
		List<InComeDto> inComeDtos = new ArrayList<>();
		if ("".equals(staffNameParam)) {
			inComeDtos = inComeService.getInComeByTime(startDateParam, endDateParam);
		} else {
			inComeDtos = inComeService.getInComeByTimeAndUser(startDateParam, endDateParam, staffNameParam);
		}
		JSONArray jsonArray = new JSONArray(inComeDtos);
		response.setContentType("application/json;charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.print(jsonArray.toString());
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/inCome/list.jsp").forward(request, response);
	}
}
