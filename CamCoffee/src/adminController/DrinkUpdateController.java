package adminController;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.DrinkEntity;
import service.DrinkService;
import serviceImpl.DrinkServiceImpl;
import utility.Constant;

@WebServlet("/drink/update")
public class DrinkUpdateController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1941067176644745182L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/drink/update.jsp").forward(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		DrinkEntity drinkEntity = new DrinkEntity();
		DrinkService drinkService = new DrinkServiceImpl();
		String nameParam = request.getParameter("name").trim();
		String idParam = request.getParameter("id");
		String drinkTypeIdParam = request.getParameter("drinkTypeId");
		String costParam = request.getParameter("cost");
		if ("".equals(nameParam) || "".equals(idParam) || "".equals(drinkTypeIdParam) || "".equals(costParam)
				|| costParam.length() > Constant.MAX_LENGTH_COST) {
			request.setAttribute(Constant.RESULT, Constant.FAILD);
		} else {
			try {
				drinkEntity.setName(nameParam);
				drinkEntity.setId(Integer.parseInt(idParam));
				drinkEntity.setDrinkTypeId(Integer.parseInt(drinkTypeIdParam));
				drinkEntity.setCost(Integer.parseInt(costParam));
				request.setAttribute(Constant.RESULT, drinkService.update(drinkEntity));
			} catch (Exception e) {
				e.printStackTrace();
				request.setAttribute(Constant.RESULT, Constant.FAILD);
			}
		}
		request.getRequestDispatcher("/drink/update.jsp").forward(request, response);
	}
}
