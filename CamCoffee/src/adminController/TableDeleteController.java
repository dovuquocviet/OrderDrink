package adminController;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.TableService;
import serviceImpl.TableServiceImpl;
import utility.Constant;

@WebServlet("/table/delete")
public class TableDeleteController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4956869956025982920L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/table/list.jsp").forward(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		TableService tableService = new TableServiceImpl();
		String paramId = request.getParameter("id").trim();
		if (!"".equals(paramId)) {
			try {
				int id = Integer.parseInt(paramId);
				tableService.delete(id);
			} catch (Exception e) {
				e.printStackTrace();
				request.setAttribute(Constant.RESULT, Constant.FAILD);
			}
		} else {
			request.setAttribute(Constant.RESULT, Constant.FAILD);
		}
		request.getRequestDispatcher("/table/list.jsp").forward(request, response);
	}
}
