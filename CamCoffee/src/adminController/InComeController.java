package adminController;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;

import dto.InComeDto;
import service.InComeService;
import serviceImpl.InComeServiceImpl;

@WebServlet("/inCome/getInCome")
public class InComeController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9191699498172412092L;
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		InComeService inComeService = new InComeServiceImpl();
		List<InComeDto> inComeDtos = inComeService.getInCome();
		JSONArray jsonArray = new JSONArray(inComeDtos);
		response.setContentType("application/json;charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.print(jsonArray.toString());
	}
}
