package entity;

import java.util.Date;

public class OrderEntity {
	private int id;
	private int drinkId;
	private String userName;
	private int tableId;
	private int billId;
	private int isCold;
	private int isServiced;

	public int getIsServiced() {
		return isServiced;
	}

	public void setIsServiced(int isServiced) {
		this.isServiced = isServiced;
	}

	private Date createTime;

	public OrderEntity() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDrinkId() {
		return drinkId;
	}

	public void setDrinkId(int drinkId) {
		this.drinkId = drinkId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getTableId() {
		return tableId;
	}

	public void setTableId(int tableId) {
		this.tableId = tableId;
	}

	public int getBillId() {
		return billId;
	}

	public void setBillId(int billId) {
		this.billId = billId;
	}

	public int getIsCold() {
		return isCold;
	}

	public void setIsCold(int isCold) {
		this.isCold = isCold;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public OrderEntity(int id, int drinkId, String userName, int tableId, int billId, int isCold, Date createTime) {
		super();
		this.id = id;
		this.drinkId = drinkId;
		this.userName = userName;
		this.tableId = tableId;
		this.billId = billId;
		this.isCold = isCold;
		this.createTime = createTime;
	}
}
