package entity;

import java.util.Date;

public class BillEntity {
	private int id;
	private Date createTime;
	private int isPay;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public int getIsPay() {
		return isPay;
	}

	public void setIsPay(int isPay) {
		this.isPay = isPay;
	}

	public BillEntity(int id, Date createTime, int isPay) {
		super();
		this.id = id;
		this.createTime = createTime;
		this.isPay = isPay;
	}

	public BillEntity() {
		super();
	}

}
