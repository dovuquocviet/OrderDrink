package entity;

public class DrinkEntity {

	public DrinkEntity() {
		super();
	}

	private int id;

	private String name;

	private int drinkTypeId;

	private int cost;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDrinkTypeId() {
		return drinkTypeId;
	}

	public void setDrinkTypeId(int drinkTypeId) {
		this.drinkTypeId = drinkTypeId;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public DrinkEntity(int id, String name, int drinkTypeId, int cost) {
		super();
		this.id = id;
		this.name = name;
		this.drinkTypeId = drinkTypeId;
		this.cost = cost;
	}
}
