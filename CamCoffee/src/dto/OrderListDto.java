package dto;

public class OrderListDto {
	private int id;
	private String drinkName;
	private int isCold;
	private String tableName;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDrinkName() {
		return drinkName;
	}

	public void setDrinkName(String drinkName) {
		this.drinkName = drinkName;
	}

	public int getIsCold() {
		return isCold;
	}

	public void setIsCold(int isCold) {
		this.isCold = isCold;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public OrderListDto(int id, String drinkName, int isCold, String tableName) {
		super();
		this.id = id;
		this.drinkName = drinkName;
		this.isCold = isCold;
		this.tableName = tableName;
	}

	public OrderListDto() {
		super();
	}

}
