package dto;

public class InComeDto {
	private int orderId;
	private String drinkName;
	private String tableName;
	private String userName;
	private String createDate;
	private int cost;

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public String getDrinkName() {
		return drinkName;
	}

	public void setDrinkName(String drinkName) {
		this.drinkName = drinkName;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public InComeDto(int orderId, String drinkName, String tableName, String userName, String createDate, int cost) {
		super();
		this.orderId = orderId;
		this.drinkName = drinkName;
		this.tableName = tableName;
		this.userName = userName;
		this.createDate = createDate;
		this.cost = cost;
	}

	public InComeDto() {
		super();
	}
}
