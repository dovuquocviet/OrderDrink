package dto;

public class OrderDto {
	private int id;
	private int tableId;
	private String tableName;
	private int drinkId;
	private String drinkName;
	private int cost;
	private int billId;
	private int isCold;

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTableId() {
		return tableId;
	}

	public void setTableId(int tableId) {
		this.tableId = tableId;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public int getDrinkId() {
		return drinkId;
	}

	public void setDrinkId(int drinkId) {
		this.drinkId = drinkId;
	}

	public String getDrinkName() {
		return drinkName;
	}

	public void setDrinkName(String drinkName) {
		this.drinkName = drinkName;
	}

	public int getBillId() {
		return billId;
	}

	public void setBillId(int billId) {
		this.billId = billId;
	}

	public int getIsCold() {
		return isCold;
	}

	public void setIsCold(int isCold) {
		this.isCold = isCold;
	}

	public OrderDto(int id, int tableId, String tableName, int drinkId, String drinkName, int cost, int billId,
			int isCold) {
		super();
		this.id = id;
		this.tableId = tableId;
		this.tableName = tableName;
		this.drinkId = drinkId;
		this.drinkName = drinkName;
		this.cost = cost;
		this.billId = billId;
		this.isCold = isCold;
	}

	public OrderDto() {
		super();
	}

}
