package service;

import java.util.List;

import entity.TableEntity;

public interface TableService {
	int create(TableEntity tableEntity);

	List<TableEntity> getAll();

	int update(TableEntity tableEntity);

	TableEntity findById(int id);

	int delete(int id);
}
