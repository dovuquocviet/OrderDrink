package service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/websocket/{user}")
public class WebSocketTest {
	private static List<Session> USERS = new ArrayList<>();

	@OnMessage
	public void onMessage(String message, Session session) throws IOException, InterruptedException {
		// System.out.println("Received: " + message);
		//
		// session.getBasicRemote().sendText("This is first message");
		//
		// int sentMessges = 0;
		// while (sentMessges < 3) {
		// Thread.sleep(500);
		// session.getBasicRemote().sendText("This is an intermediate server message.
		// Count: " + sentMessges);
		// sentMessges++;
		// }
		//
		// session.getBasicRemote().sendText("This is the last server message");
		// Set<Session> listSession = session.getOpenSessions();
		// for (Session s : listSession) {
		// if (!s.getUserProperties().get("user").equals("viet")) {
		// s.getAsyncRemote().sendText(message);
		// }
		// }
		for (Session s : USERS) {
			if (s != session) {
				s.getAsyncRemote().sendText(message);
			}
		}
	}

	@OnOpen
	public void onOpen(@PathParam("user") String user, Session s) {
		s.getUserProperties().put("user", user);
		System.out.println(user);
		USERS.add(s);
	}

	@OnClose
	public void onClose(Session s) {
		System.out.println("Connection closed");
		if (s != null) {
			USERS.remove(s);
		}
	}
}
