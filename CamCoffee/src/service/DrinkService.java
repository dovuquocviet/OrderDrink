package service;

import java.util.List;

import dto.DrinkDto;
import entity.DrinkEntity;

public interface DrinkService {

	List<DrinkDto> getAll();

	int create(DrinkEntity drinkEntity);

	DrinkDto findById(int id);

	int update(DrinkEntity drinkEntity);

	int delete(int id);

	List<DrinkEntity> findByDrinkTypeId(int drinkTypeId);
}
