package service;

import java.util.List;

import org.json.JSONObject;

import dto.OrderDto;
import dto.OrderListDto;

public interface OrderService {
	int create(JSONObject jsonObject, String userName);

	List<OrderDto> findOrderByTableId(int tableId);

	List<OrderListDto> findOrder();

	int serviced(int id);

	int findBillId(int id);
}
