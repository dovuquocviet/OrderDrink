package service;

public interface BillService {
	int create();

	int pay(int id);
}
