package service;

import java.util.List;

import entity.DrinkTypeEntity;

public interface DrinkTypeService {
	int create(DrinkTypeEntity drinkTypeEntity);

	List<DrinkTypeEntity> getAll();

	int update(DrinkTypeEntity drinkTypeEntity);

	DrinkTypeEntity findById(int id);

	int delete(int id);
}
