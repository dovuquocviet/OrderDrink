package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import service.OrderService;
import serviceImpl.OrderServiceImpl;
import utility.Constant;

@WebServlet("/order/order")
public class OrderController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6864967624849791701L;

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OrderService orderService = new OrderServiceImpl();
		try {
			JSONObject jsonObject = new JSONObject(request.getParameter(Constant.ORDERS));
			orderService.create(jsonObject, request.getSession().getAttribute(Constant.USER_ENTITY).toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		request.getRequestDispatcher("/order/order.jsp").forward(request, response);
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/order/order.jsp").forward(request, response);
	}
}
