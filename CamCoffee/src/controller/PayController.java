package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.BillService;
import service.OrderService;
import serviceImpl.BillServiceImpl;
import serviceImpl.OrderServiceImpl;
import utility.Constant;

@WebServlet("/bill/pay")
public class PayController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5810335358275272061L;

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int tableId = 0;
		try {
			int orderId = Integer.parseInt(request.getParameter("orderId"));
			tableId = Integer.parseInt(request.getParameter("tableId"));
			BillService billService = new BillServiceImpl();
			OrderService orderService = new OrderServiceImpl();
			billService.pay(orderService.findBillId(orderId));
			request.setAttribute(Constant.RESULT, Constant.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute(Constant.RESULT, Constant.FAILD);
		}
		request.getRequestDispatcher("/order/table.jsp?tableId" + tableId).forward(request, response);
	}
}
