package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.OrderService;
import serviceImpl.OrderServiceImpl;

@WebServlet("/order/serviced")
public class OrderServicedController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8101997343193005941L;

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			int id = Integer.parseInt(request.getParameter("id"));
			OrderService orderService = new OrderServiceImpl();
			orderService.serviced(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		request.getRequestDispatcher("/order/list.jsp").forward(request, response);
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/order/list.jsp").forward(request, response);
	}
}
