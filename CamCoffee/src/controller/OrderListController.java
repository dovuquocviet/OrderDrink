package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;

import dto.OrderListDto;
import service.OrderService;
import serviceImpl.OrderServiceImpl;

@WebServlet("/order/list")
public class OrderListController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6864967624849791701L;

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String idParam = request.getParameter("ids");
		try {
			JSONArray ids = new JSONArray(idParam);
			int size = ids.length();
			OrderService orderService = new OrderServiceImpl();
			List<OrderListDto> orderListDtos = orderService.findOrder();
			List<OrderListDto> result = new ArrayList<>();

			for (OrderListDto orderListDto : orderListDtos) {
				boolean flag = true;
				for (int i = 0; i < size; i++) {
					int id = Integer.parseInt(ids.getJSONObject(i).get("id").toString());
					if (id == orderListDto.getId()) {
						flag = false;
						break;
					}
				}
				if (flag)
					result.add(orderListDto);
			}
			JSONArray jsonArray = new JSONArray(result);
			response.setContentType("application/json;charset=UTF-8");
			PrintWriter out = response.getWriter();
			out.print(jsonArray.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/order/list.jsp").forward(request, response);
	}
}
