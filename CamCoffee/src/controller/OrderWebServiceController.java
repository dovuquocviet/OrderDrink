package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import org.json.JSONException;
import org.json.JSONObject;

import service.OrderService;
import serviceImpl.OrderServiceImpl;
import utility.Constant;

@ServerEndpoint("/orderWebService/{user}/{screenId}")
public class OrderWebServiceController {
	private static List<Session> LIST_SESSION = new ArrayList<>();

	@OnMessage
	public void onMessage(String message, Session session) throws IOException, InterruptedException {
		System.out.println(session.getUserProperties().get("screenId"));
		System.out.println(session.getUserProperties().get("user"));
		System.out.println(message);
		OrderService orderService = new OrderServiceImpl();
		try {
			JSONObject jsonObject = new JSONObject(message);
			if (session.getUserProperties().get("screenId").toString().equals(Constant.ORDER_SCREEN)) {
				int result = orderService.create(jsonObject, session.getUserProperties().get("user").toString());
				if (result == Constant.SUCCESS) {
					for (Session s : LIST_SESSION) {
						s.getAsyncRemote().sendText(String.valueOf(Constant.SUCCESS));
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@OnOpen
	public void onOpen(@PathParam("user") String user, @PathParam("screenId") String screenId, Session s) {
		s.getUserProperties().put("screenId", screenId);
		s.getUserProperties().put("user", user);
		LIST_SESSION.add(s);
	}

	@OnClose
	public void onClose(Session s) {
		System.out.println("Connection closed");
		if (s != null) {
			LIST_SESSION.remove(s);
		}
	}
}
