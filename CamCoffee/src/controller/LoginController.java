package controller;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import entity.UserEntity;
import service.UserService;
import serviceImpl.UserServiceImpl;
import utility.Constant;

@WebServlet("/login/*")
public class LoginController extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -506825035538407676L;

	public static List<HttpSession> USERS = new ArrayList<>();
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute(Constant.USER_ENTITY) != null) {
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		} else {
			request.getRequestDispatcher("/login.jsp").forward(request, response);
		}
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String userName = request.getParameter("userName").trim();
		String password = request.getParameter("password").trim();
		if (userName.length() == 0 || password.length() == 0) {
			request.setAttribute(Constant.RESULT, Constant.FAILD);
			request.getRequestDispatcher("/login.jsp").forward(request, response);
		} else {
			UserService userService = new UserServiceImpl();
			try {
				MessageDigest md = MessageDigest.getInstance("MD5");
				md.update(password.getBytes());
				byte[] digest = md.digest();
				UserEntity userEntity = new UserEntity();
				String passwordMD5 = DatatypeConverter.printHexBinary(digest).toUpperCase();
				userEntity.setUserName(userName);
				userEntity.setPassword(passwordMD5);
				if (userService.getUser(userEntity) == Constant.SUCCESS) {
					HttpSession session = request.getSession();
					session.setAttribute(Constant.USER_ENTITY, userName);
					USERS.add(session);
					request.getRequestDispatcher("/index.jsp").forward(request, response);
				} else {
					request.setAttribute(Constant.RESULT, Constant.FAILD);
					request.getRequestDispatcher("/login.jsp").forward(request, response);
				}
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
		}
	}
}
