package dao;

import java.util.List;

import entity.DrinkTypeEntity;

public interface DrinkTypeDao extends BaseDao {
	List<DrinkTypeEntity> getAll();

	int create(DrinkTypeEntity drinkTypeEntity);

	int update(DrinkTypeEntity drinkTypeEntity);

	DrinkTypeEntity findById(int id);

	int delete(int id);
}
