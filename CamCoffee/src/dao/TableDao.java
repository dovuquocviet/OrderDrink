package dao;

import java.util.List;

import entity.TableEntity;

public interface TableDao extends BaseDao {
	List<TableEntity> getAll();

	int create(TableEntity tableEntity);

	int update(TableEntity tableEntity);

	TableEntity findById(int id);

	int delete(int id);
}
