package dao;

import java.sql.Connection;

public interface BaseDao {
	public Connection getConnection();
}
