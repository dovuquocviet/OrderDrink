package dao;

import java.util.List;

import dto.DrinkDto;
import entity.DrinkEntity;

public interface DrinkDao extends BaseDao {
	List<DrinkEntity> findByDrinkTypeId(int drinkTypeId);

	List<DrinkDto> getAll();

	int create(DrinkEntity drinkEntity);

	DrinkDto findById(int id);

	int update(DrinkEntity drinkEntity);

	int delete(int id);
}
