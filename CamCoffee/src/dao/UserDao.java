package dao;

import java.util.List;

import entity.UserEntity;

public interface UserDao extends BaseDao {
	int create(UserEntity userEntity);

	int getUser(UserEntity userEntity);

	List<UserEntity> getAll();

	int lock(int id);

	int unlock(int id);

	UserEntity findById(int id);

	int update(UserEntity userEntity);
}
