package dao;

import java.util.List;

import dto.OrderDto;
import dto.OrderListDto;
import entity.OrderEntity;

public interface OrderDao extends BaseDao {
	int create(OrderEntity orderEntity);

	List<OrderDto> findOrderByTableId(int tableId);

	List<OrderListDto> findOrder();

	int serviced(int id);

	int findBillId(int id);
}
