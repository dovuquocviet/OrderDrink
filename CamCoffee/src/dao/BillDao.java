package dao;

import java.util.List;

import entity.BillEntity;

public interface BillDao extends BaseDao {
	List<BillEntity> getAll();

	int create(BillEntity billEntity);

	BillEntity findById(int id);

	int pay(int id);

	int delete(int id);
}
