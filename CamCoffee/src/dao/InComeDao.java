package dao;

import java.util.List;

import dto.InComeDto;

public interface InComeDao extends BaseDao {
	List<InComeDto> getInCome();

	List<InComeDto> getInComeByTime(String startDate, String endDate);

	List<InComeDto> getInComeByTimeAndUser(String startDate, String endDate, String userName);

}
