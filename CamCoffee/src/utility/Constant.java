package utility;

public class Constant {
	public static final int SUCCESS = 1;
	public static final int FAILD = 0;
	public static final int MAX_LENGTH_COST = 6;
	public static final String ddMMyyyy = "dd/MM/yyyy";
	public static final String ddMMyyyyHHmmss = "dd/MM/yyyy HH:mm:ss";
	public static final String YYYYMMddHHmmss = "YYYY/MM/dd HH:mm:ss";

	public static final String ERROR = "error";
	public static final String CODE = "code";

	public static final String USER_ENTITY = "userEntity";
	public static final String ADMIN = "admin";
	public static final String RESULT = "result";

	public static final String MESS_SUCCESS = "Thao Tác Thành Công";
	public static final String MESS_FAILD = "Thao Tác Thất Bại";
	public static final String MESS_CONFIRM = "Bạn Có Chắc Muốn Xóa Không?";
	public static final String MESS_LOCK = "Bạn Có Chắc Muốn Khóa Tài Khoản Này Không?";
	public static final String MESS_UNLOCK = "Bạn Có Chắc Muốn Mở Khóa Tài Khoản Này Không?";
	public static final String MESS_LOGIN_FAILD = "Đăng Nhập Thất Bại";
	public static final String MESS_CONFIRM_PAY = "Bạn Có Muốn Thanh Toán Không?";

	public static final int DELETE = 1;
	public static final int NOT_DELTE = 0;

	public static final int IS_NOT_PAY = 0;
	public static final int IS_PAY = 1;

	public static final String DRINK_TYPE = "Loại Đồ Uống";
	public static final String DRINK = "Đồ Uống";
	public static final String USER = "Nhân Viên";
	public static final String ORDER = "Gọi Đồ";
	public static final String SETTING = "Cài Đặt";
	public static final String MONEY = "Doanh Thu";
	public static final String LOGOUT = "Đăng Xuất";
	public static final String TABLE = "Bàn";
	public static final String LIST_ORDER = "Danh Sách Gọi Đồ";

	public static final String LIST = "list";
	public static final String ADD_NEW = "Thêm Mới";
	public static final String HOME = "Trang Chủ";
	public static final String UPDATE = "Cập Nhật";
	public static final String NAME = "Tên";
	public static final String USER_NAME = "Tên Đăng Nhập";
	public static final String RE_PASSWORD = "Nhập Lại Mật Khẩu";
	public static final String PASSWORD = "Mật Khẩu";
	public static final String TYPE = "Loại";
	public static final String COST = "Giá";
	public static final String EDIT = "Sửa";
	public static final String REGISTER = "Đăng Ký";
	public static final String REMOVE = "Xóa";
	public static final String LOCK = "Khóa";
	public static final String UNLOCK = "Mở";
	public static final String BACK = "Quay Lại";
	public static final String ID = "Mã";
	public static final String YES = "Có";
	public static final String NO = "Không";
	public static final String HOT = "Nóng";
	public static final String COLD = "Đá";
	public static final String TOTAL_AMOUNT = "Tiền";
	public static final String PAY = "Thanh Toán";
	public static final String CHOOSE_TABLE = "Chọn Bàn";
	public static final String LIST_ORDERS = "Danh Sách Gọi Đồ";
	public static final String SERVICED = "Phục Vụ";
	public static final String DATE = "Ngày";
	public static final String START_DATE = "Bắt Đầu";
	public static final String END_DATE = "Kết Thúc";
	public static final String STAFF = "Nhân Viên";
	public static final String SEARCH = "Tìm Kiếm";
	public static final String INCOME_MONTH = "Doanh Thu Tháng";

	public static final String TABLE_ID = "tableId";
	public static final String DRINKS = "drinks";
	public static final String ID_DRINK = "drinkId";
	public static final String TYPE_VAL = "typeVal";
	public static final String ORDERS = "orders";

	public static final String ORDER_SCREEN = "order";
	public static final String LIST_ORDER_SCREEN = "LIST_ORDER_SCREEN";

}
