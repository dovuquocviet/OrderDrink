package utility;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {
	public static DBConnection dbConnection;
	private Connection connection;

	private DBConnection() {
		String driver = "com.mysql.jdbc.Driver";
		String hostName = "127.0.0.1:3306";
		String dbName = "coffee_shop";
		String userName = "root";
		String password = "";
		try {
			Class.forName(driver).newInstance();
			String connectionURL = "jdbc:mysql://" + hostName + "/" + dbName
					+ "?useUnicode=yes&characterEncoding=UTF-8";
			connection = DriverManager.getConnection(connectionURL, userName, password);
		} catch (Exception sqle) {
			sqle.printStackTrace();
		}
	}

	public static DBConnection getInstance() {
		if (dbConnection == null) {
			dbConnection = new DBConnection();
		}
		return dbConnection;

	}

	public Connection getConnection() {
		return connection;
	}
}
