package daoImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dao.DrinkTypeDao;
import entity.DrinkTypeEntity;
import utility.Constant;

public class DrinkTypeDaoImpl extends BaseDaoImpl implements DrinkTypeDao {

	@Override
	public List<DrinkTypeEntity> getAll() {
		List<DrinkTypeEntity> drinkTypeEntities = new ArrayList<>();
		Connection connection = getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM drink_type WHERE is_delete = ? ORDER BY id";
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, Constant.NOT_DELTE);
			rs = ps.executeQuery();
			while (rs.next()) {
				int id = Integer.parseInt(rs.getString("id"));
				String name = rs.getString("name");
				DrinkTypeEntity drinkTypeEntity = new DrinkTypeEntity(id, name);
				drinkTypeEntities.add(drinkTypeEntity);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return drinkTypeEntities;
	}

	@Override
	public int create(DrinkTypeEntity drinkTypeEntity) {
		Connection connection = getConnection();
		String sql = "INSERT INTO drink_type(`name`) VALUES (?)";
		PreparedStatement ps = null;
		int result = 0;
		try {
			ps = connection.prepareStatement(sql);
			ps.setString(1, drinkTypeEntity.getName());
			result = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return Constant.FAILD;
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public int update(DrinkTypeEntity drinkTypeEntity) {
		Connection connection = getConnection();
		String sql = "UPDATE drink_type SET `name` = ? WHERE id = ?";
		PreparedStatement ps = null;
		int result = 0;
		try {
			ps = connection.prepareStatement(sql);
			ps.setString(1, drinkTypeEntity.getName());
			ps.setInt(2, drinkTypeEntity.getId());
			result = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return Constant.FAILD;
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public DrinkTypeEntity findById(int id) {
		Connection connection = getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		DrinkTypeEntity drinkTypeEntity = null;
		String sql = "SELECT * FROM drink_type WHERE id = ? AND is_delete = 0";
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			if (rs != null) {
				rs.next();
				String name = rs.getString("name");
				drinkTypeEntity = new DrinkTypeEntity(id, name);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return drinkTypeEntity;
	}

	@Override
	public int delete(int id) {
		Connection connection = getConnection();
		String sql = "UPDATE drink_type SET `is_delete` = ? WHERE id = ?";
		PreparedStatement ps = null;
		int result = 0;
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, Constant.DELETE);
			ps.setInt(2, id);
			result = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return Constant.FAILD;
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
}
