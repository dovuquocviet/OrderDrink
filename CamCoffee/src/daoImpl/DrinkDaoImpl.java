package daoImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dao.DrinkDao;
import dto.DrinkDto;
import entity.DrinkEntity;
import utility.Constant;

public class DrinkDaoImpl extends BaseDaoImpl implements DrinkDao {

	@Override
	public List<DrinkEntity> findByDrinkTypeId(int drinkTypeId) {
		List<DrinkEntity> drinkEntities = new ArrayList<>();
		Connection connection = getConnection();
		String sql = "SELECT d.* FROM drink d INNER JOIN drink_type dt ON d.drink_type_id = dt.id WHERE dt.id = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, drinkTypeId);
			rs = ps.executeQuery();
			while (rs.next()) {
				int id = Integer.parseInt(rs.getString("id"));
				String name = rs.getString("name");
				int cost = Integer.parseInt(rs.getString("cost"));
				DrinkEntity drinkDto = new DrinkEntity(id, name, drinkTypeId, cost);
				drinkEntities.add(drinkDto);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return drinkEntities;
	}

	@Override
	public List<DrinkDto> getAll() {
		List<DrinkDto> drinkDtos = new ArrayList<>();
		Connection connection = getConnection();
		String sql = "SELECT d.id, d.name, d.drink_type_id, d.cost, dt.name AS drink_type_name FROM drink d INNER JOIN drink_type dt ON d.drink_type_id = dt.id WHERE dt.is_delete = 0 AND d.is_delete = 0";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				int id = Integer.parseInt(rs.getString("id"));
				String name = rs.getString("name");
				int drinkTypeId = Integer.parseInt(rs.getString("drink_type_id"));
				int cost = Integer.parseInt(rs.getString("cost"));
				String drinkTypeName = rs.getString("drink_type_name");
				DrinkDto drinkDto = new DrinkDto(id, name, drinkTypeId, cost, drinkTypeName);
				drinkDtos.add(drinkDto);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return drinkDtos;
	}

	@Override
	public int create(DrinkEntity drinkEntity) {
		Connection connection = getConnection();
		String sql = "INSERT INTO drink(`name`, `drink_type_id`, `cost`) VALUES (?, ?, ?)";
		PreparedStatement ps = null;
		int result = 0;
		try {
			ps = connection.prepareStatement(sql);
			ps.setString(1, drinkEntity.getName());
			ps.setInt(2, drinkEntity.getDrinkTypeId());
			ps.setInt(3, drinkEntity.getCost());
			result = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return Constant.FAILD;
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public DrinkDto findById(int id) {
		Connection connection = getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		DrinkDto drinkDto = null;
		String sql = "SELECT d.id, d.name, d.drink_type_id, d.cost, dt.name AS drink_type_name FROM drink d INNER JOIN drink_type dt ON d.drink_type_id = dt.id WHERE d.id = ? AND dt.is_delete = 0 AND d.is_delete = 0";
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			if (rs != null) {
				rs.next();
				String name = rs.getString("name");
				int drinkTypeId = Integer.parseInt(rs.getString("drink_type_id"));
				int cost = Integer.parseInt(rs.getString("cost"));
				String drinkTypeName = rs.getString("drink_type_name");
				drinkDto = new DrinkDto(id, name, drinkTypeId, cost, drinkTypeName);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return drinkDto;
	}

	@Override
	public int update(DrinkEntity drinkEntity) {
		Connection connection = getConnection();
		String sql = "UPDATE drink SET `name` = ?, `drink_type_id` = ?, `cost` = ? WHERE id = ?";
		PreparedStatement ps = null;
		int result = 0;
		try {
			ps = connection.prepareStatement(sql);
			ps.setString(1, drinkEntity.getName());
			ps.setInt(2, drinkEntity.getDrinkTypeId());
			ps.setInt(3, drinkEntity.getCost());
			ps.setInt(4, drinkEntity.getId());
			result = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return Constant.FAILD;
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public int delete(int id) {
		Connection connection = getConnection();
		String sql = "UPDATE drink SET `is_delete` = 1 WHERE id = ?";
		PreparedStatement ps = null;
		int result = 0;
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			result = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return Constant.FAILD;
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
}
