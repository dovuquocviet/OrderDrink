package daoImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dao.InComeDao;
import dto.InComeDto;

public class InComeDaoImpl extends BaseDaoImpl implements InComeDao {

	@Override
	public List<InComeDto> getInCome() {
		List<InComeDto> inComeDtos = new ArrayList<>();
		Connection connection = getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT o.id, d.name AS drink_name, t.name AS table_name, o.user_name, DATE_FORMAT(b.create_time, '%d/%m/%Y %H:%i') AS create_time, d.cost FROM bill b INNER JOIN `order` o ON b.id = o.bill_id INNER JOIN drink d ON o.drink_id = d.id INNER JOIN tables t ON o.table_id = t.id WHERE MONTH(b.create_time) = MONTH(NOW()) AND YEAR(b.create_time) = YEAR(NOW()) AND b.is_pay = 1";
		try {
			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				int orderId = Integer.parseInt(rs.getString("id"));
				String drinkName = rs.getString("drink_name");
				String tableName = rs.getString("table_name");
				String userName = rs.getString("user_name");
				String date = rs.getString("create_time");
				int cost = Integer.parseInt(rs.getString("cost"));
				InComeDto inComeDto = new InComeDto(orderId, drinkName, tableName, userName, date, cost);
				inComeDtos.add(inComeDto);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return inComeDtos;
	}

	@Override
	public List<InComeDto> getInComeByTime(String startDate, String endDate) {
		List<InComeDto> inComeDtos = new ArrayList<>();
		Connection connection = getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT o.id, d.name AS drink_name, t.name AS table_name, o.user_name, DATE_FORMAT(b.create_time, '%d/%m/%Y %H:%i') AS create_time, d.cost FROM bill b INNER JOIN `order` o ON b.id = o.bill_id INNER JOIN drink d ON o.drink_id = d.id INNER JOIN tables t ON o.table_id = t.id WHERE (DATE_FORMAT(b.create_time, '%d/%m/%Y') BETWEEN ? AND ?) AND b.is_pay = 1";
		try {
			ps = connection.prepareStatement(sql);
			ps.setString(1, startDate);
			ps.setString(2, endDate);
			rs = ps.executeQuery();
			while (rs.next()) {
				int orderId = Integer.parseInt(rs.getString("id"));
				String drinkName = rs.getString("drink_name");
				String tableName = rs.getString("table_name");
				String userName = rs.getString("user_name");
				String date = rs.getString("create_time");
				int cost = Integer.parseInt(rs.getString("cost"));
				InComeDto inComeDto = new InComeDto(orderId, drinkName, tableName, userName, date, cost);
				inComeDtos.add(inComeDto);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return inComeDtos;
	}

	@Override
	public List<InComeDto> getInComeByTimeAndUser(String startDate, String endDate, String userName) {
		List<InComeDto> inComeDtos = new ArrayList<>();
		Connection connection = getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT o.id, d.name AS drink_name, t.name AS table_name, o.user_name, DATE_FORMAT(b.create_time, '%d/%m/%Y %H:%i') AS create_time, d.cost FROM bill b INNER JOIN `order` o ON b.id = o.bill_id INNER JOIN drink d ON o.drink_id = d.id INNER JOIN tables t ON o.table_id = t.id WHERE (DATE_FORMAT(b.create_time, '%d/%m/%Y') BETWEEN ? AND ?) AND o.user_name = ? AND b.is_pay = 1";
		try {
			ps = connection.prepareStatement(sql);
			ps.setString(1, startDate);
			ps.setString(2, endDate);
			ps.setString(3, userName);
			rs = ps.executeQuery();
			while (rs.next()) {
				int orderId = Integer.parseInt(rs.getString("id"));
				String drinkName = rs.getString("drink_name");
				String tableName = rs.getString("table_name");
				String date = rs.getString("create_time");
				int cost = Integer.parseInt(rs.getString("cost"));
				InComeDto inComeDto = new InComeDto(orderId, drinkName, tableName, userName, date, cost);
				inComeDtos.add(inComeDto);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return inComeDtos;
	}

}
