package daoImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mysql.jdbc.Statement;

import dao.BillDao;
import entity.BillEntity;
import utility.Constant;

public class BillDaoImpl extends BaseDaoImpl implements BillDao {

	@Override
	public List<BillEntity> getAll() {
		List<BillEntity> billEntities = new ArrayList<>();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constant.ddMMyyyy);
		Connection connection = getConnection();
		String sql = "SELECT * FROM bill";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				int id = Integer.parseInt(rs.getString("id"));
				Date createTime = simpleDateFormat.parse(rs.getString("create_time"));
				int isPay = Integer.parseInt(rs.getString("is_pay"));
				BillEntity billEntity = new BillEntity(id, createTime, isPay);
				billEntities.add(billEntity);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return billEntities;
	}

	@Override
	public int create(BillEntity billEntity) {
		Connection connection = getConnection();
		String sql = "INSERT INTO bill(`create_time`, `is_pay`) VALUES (?, ?)";
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		int result = 0;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constant.YYYYMMddHHmmss);
		try {
			String createTime = simpleDateFormat.format(billEntity.getCreateTime()).toString();
			ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, createTime);
			ps.setInt(2, billEntity.getIsPay());
			ps.executeUpdate();
			resultSet = ps.getGeneratedKeys();
			resultSet.next();
			result = resultSet.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
			return Constant.FAILD;
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public BillEntity findById(int id) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constant.ddMMyyyy);
		Connection connection = getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		BillEntity billEntity = null;
		String sql = "SELECT * FROM bill WHERE id = ?";
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			if (rs != null) {
				rs.next();
				Date createTime = simpleDateFormat.parse(rs.getString("create_time"));
				int isPay = Integer.parseInt(rs.getString("is_pay"));
				billEntity = new BillEntity(id, createTime, isPay);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return billEntity;
	}

	@Override
	public int pay(int id) {
		Connection connection = getConnection();
		String sql = "UPDATE bill SET `is_pay` = 1 WHERE id = ?";
		PreparedStatement ps = null;
		int result = 0;
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			result = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return Constant.FAILD;
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return 0;
	}
}
