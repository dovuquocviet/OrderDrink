package daoImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dao.TableDao;
import entity.TableEntity;
import utility.Constant;

public class TableDaoImp extends BaseDaoImpl implements TableDao {

	@Override
	public List<TableEntity> getAll() {
		List<TableEntity> tableEntities = new ArrayList<>();
		Connection connection = getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM tables WHERE is_delete = ? ORDER BY id";
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, Constant.NOT_DELTE);
			rs = ps.executeQuery();
			while (rs.next()) {
				int id = Integer.parseInt(rs.getString("id"));
				String name = rs.getString("name");
				TableEntity tableEntity = new TableEntity(id, name);
				tableEntities.add(tableEntity);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return tableEntities;
	}

	@Override
	public int create(TableEntity tableEntity) {
		Connection connection = getConnection();
		String sql = "INSERT INTO tables(`name`) VALUES (?)";
		PreparedStatement ps = null;
		int result = 0;
		try {
			ps = connection.prepareStatement(sql);
			ps.setString(1, tableEntity.getName());
			result = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return Constant.FAILD;
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public int update(TableEntity tableEntity) {
		Connection connection = getConnection();
		String sql = "UPDATE tables SET `name` = ? WHERE id = ?";
		PreparedStatement ps = null;
		int result = 0;
		try {
			ps = connection.prepareStatement(sql);
			ps.setString(1, tableEntity.getName());
			ps.setInt(2, tableEntity.getId());
			result = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return Constant.FAILD;
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public TableEntity findById(int id) {
		Connection connection = getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		TableEntity tableEntity = null;
		String sql = "SELECT * FROM tables WHERE id = ? AND is_delete = 0";
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			if (rs != null) {
				rs.next();
				String name = rs.getString("name");
				tableEntity = new TableEntity(id, name);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return tableEntity;
	}

	@Override
	public int delete(int id) {
		Connection connection = getConnection();
		String sql = "UPDATE tables SET `is_delete` = ? WHERE id = ?";
		PreparedStatement ps = null;
		int result = 0;
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, Constant.DELETE);
			ps.setInt(2, id);
			result = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return Constant.FAILD;
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
}
