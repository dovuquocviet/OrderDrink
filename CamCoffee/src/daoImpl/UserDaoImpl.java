package daoImpl;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import dao.UserDao;
import entity.UserEntity;
import utility.Constant;

public class UserDaoImpl extends BaseDaoImpl implements UserDao {

	@Override
	public int create(UserEntity userEntity) {
		Connection connection = getConnection();
		String sql = "INSERT INTO user(`user_name`,`password`) VALUES (?,?)";
		PreparedStatement ps = null;
		int result = 0;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(userEntity.getPassword().getBytes());
			byte[] digest = md.digest();
			String passwordMD5 = DatatypeConverter.printHexBinary(digest).toUpperCase();

			ps = connection.prepareStatement(sql);
			ps.setString(1, userEntity.getUserName());
			ps.setString(2, passwordMD5);
			result = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return Constant.FAILD;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return Constant.FAILD;
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public int getUser(UserEntity userEntity) {
		Connection connection = getConnection();
		String sql = "SELECT * FROM user WHERE user_name = ? AND password = ? AND is_delete = 0";
		PreparedStatement ps = null;
		ResultSet rs = null;
		int result = 0;
		try {
			ps = connection.prepareStatement(sql);
			ps.setString(1, userEntity.getUserName());
			ps.setString(2, userEntity.getPassword());
			rs = ps.executeQuery();
			if (rs.next()) {
				result = Constant.SUCCESS;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return Constant.FAILD;
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public List<UserEntity> getAll() {
		List<UserEntity> userEntities = new ArrayList<>();
		Connection connection = getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM user WHERE user_name != 'admin' ORDER BY id";
		try {
			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				int id = Integer.parseInt(rs.getString("id"));
				String name = rs.getString("user_name");
				int isDelete = Integer.parseInt(rs.getString("is_delete"));
				UserEntity userEntity = new UserEntity();
				userEntity.setId(id);
				userEntity.setUserName(name);
				userEntity.setIsDelete(isDelete);
				userEntities.add(userEntity);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return userEntities;
	}

	@Override
	public int lock(int id) {
		Connection connection = getConnection();
		String sql = "UPDATE user SET `is_delete` = 1 WHERE id = ? AND user_name != 'admin'";
		PreparedStatement ps = null;
		int result = 0;
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			result = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return Constant.FAILD;
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public int unlock(int id) {
		Connection connection = getConnection();
		String sql = "UPDATE user SET `is_delete` = 0 WHERE id = ? AND user_name != 'admin'";
		PreparedStatement ps = null;
		int result = 0;
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			result = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return Constant.FAILD;
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public UserEntity findById(int id) {
		Connection connection = getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		UserEntity userEntity = null;
		String sql = "SELECT * FROM user WHERE id = ? AND user_name != 'admin'";
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			if (rs != null) {
				rs.next();
				String userName = rs.getString("user_name");
				userEntity = new UserEntity();
				userEntity.setUserName(userName);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return userEntity;
	}

	@Override
	public int update(UserEntity userEntity) {
		Connection connection = getConnection();
		String sql = "UPDATE user SET `password` = ? WHERE id = ? AND user_name != 'admin'";
		PreparedStatement ps = null;
		int result = 0;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(userEntity.getPassword().getBytes());
			byte[] digest = md.digest();
			String passwordMD5 = DatatypeConverter.printHexBinary(digest).toUpperCase();

			ps = connection.prepareStatement(sql);
			ps.setString(1, passwordMD5);
			ps.setInt(2, userEntity.getId());
			result = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return Constant.FAILD;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return Constant.FAILD;
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
}
