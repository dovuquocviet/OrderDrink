package daoImpl;

import java.sql.Connection;

import dao.BaseDao;
import utility.DBConnection;

public class BaseDaoImpl implements BaseDao {

	@Override
	public Connection getConnection() {
		DBConnection dbConnection = DBConnection.getInstance();
		return dbConnection.getConnection();
	}

}
