package daoImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import dao.OrderDao;
import dto.OrderDto;
import dto.OrderListDto;
import entity.OrderEntity;
import utility.Constant;

public class OrderDaoImpl extends BaseDaoImpl implements OrderDao {

	@Override
	public int create(OrderEntity orderEntity) {
		Connection connection = getConnection();
		String sql = "INSERT INTO `order` (`table_id`, `drink_id`, `user_name`, `bill_id`, `is_cold`, `create_time`) VALUES (?, ?, ?, ?, ?, ?)";
		PreparedStatement ps = null;
		int result = 0;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constant.YYYYMMddHHmmss);
		try {
			String createTime = simpleDateFormat.format(orderEntity.getCreateTime()).toString();
			ps = connection.prepareStatement(sql);
			ps.setInt(1, orderEntity.getTableId());
			ps.setInt(2, orderEntity.getDrinkId());
			ps.setString(3, orderEntity.getUserName());
			ps.setInt(4, orderEntity.getBillId());
			ps.setInt(5, orderEntity.getIsCold());
			ps.setString(6, createTime);
			result = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return Constant.FAILD;
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public List<OrderDto> findOrderByTableId(int tableId) {
		List<OrderDto> orderDtos = new ArrayList<>();
		Connection connection = getConnection();
		String sql = "SELECT o.id, o.table_id, t.name AS table_name, o.id AS drink_id, d.name AS drink_name, d.cost, o.user_name, o.bill_id, o.is_cold FROM `order` o INNER JOIN tables t ON o.table_id = t.id INNER JOIN drink d ON o.drink_id = d.id INNER JOIN bill b ON o.bill_id = b.id WHERE o.table_id = ? AND b.is_pay = 0 ORDER BY id";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, tableId);
			rs = ps.executeQuery();
			while (rs.next()) {
				int id = Integer.parseInt(rs.getString("id"));
				String tableName = rs.getString("table_name");
				int drinkId = Integer.parseInt(rs.getString("drink_id"));
				String drinkName = rs.getString("drink_name");
				int cost = Integer.parseInt(rs.getString("cost"));
				int billId = Integer.parseInt(rs.getString("bill_id"));
				int isCold = Integer.parseInt(rs.getString("is_cold"));
				OrderDto orderDto = new OrderDto(id, tableId, tableName, drinkId, drinkName, cost, billId, isCold);
				orderDtos.add(orderDto);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return orderDtos;
	}

	@Override
	public List<OrderListDto> findOrder() {
		List<OrderListDto> orderListDtos = new ArrayList<>();
		Connection connection = getConnection();
		String sql = "SELECT o.id, t.name AS table_name, d.name AS drink_name, o.is_cold FROM `order` o INNER JOIN tables t ON o.table_id = t.id INNER JOIN drink d ON o.drink_id = d.id WHERE o.is_serviced = 0 ORDER BY id";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				int id = Integer.parseInt(rs.getString("id"));
				String tableName = rs.getString("table_name");
				String drinkName = rs.getString("drink_name");
				int isCold = Integer.parseInt(rs.getString("is_cold"));
				OrderListDto orderListDto = new OrderListDto(id, drinkName, isCold, tableName);
				orderListDtos.add(orderListDto);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return orderListDtos;
	}

	@Override
	public int serviced(int id) {
		Connection connection = getConnection();
		String sql = "UPDATE `order` SET `is_serviced` = 1 WHERE id = ?";
		PreparedStatement ps = null;
		int result = 0;
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			result = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return Constant.FAILD;
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public int findBillId(int id) {
		Connection connection = getConnection();
		String sql = "SELECT o.bill_id FROM `order` o WHERE o.id = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			while (rs.next()) {
				int billId = Integer.parseInt(rs.getString("bill_id"));
				return billId;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return Constant.FAILD;
	}
}
